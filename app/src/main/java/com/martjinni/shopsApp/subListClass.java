package com.martjinni.shopsApp;

public class subListClass {
    private int noOfDays, isActive, subPrice, subId;

    public subListClass(int noOfDays, int isActive, int subPrice, int subId) {
        this.noOfDays = noOfDays;
        this.isActive = isActive;
        this.subPrice = subPrice;
        this.subId = subId;
    }

    public int getNoOfDays() {
        return noOfDays;
    }

    public int getIsActive() {
        return isActive;
    }

    public int getSubPrice() {
        return subPrice;
    }

    public int getSubId() {
        return subId;
    }
}
