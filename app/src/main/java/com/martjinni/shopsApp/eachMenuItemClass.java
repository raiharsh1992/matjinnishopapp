package com.martjinni.shopsApp;

import java.util.List;

public class eachMenuItemClass {
    private List<quanListClass> quantUse;
    private String itemName;
    private int isActive, subItemId, mItemId, isImage;

    public eachMenuItemClass(List<quanListClass> quantUse, String itemName, int isActive, int subItemId, int mItemId, int isImage) {
        this.quantUse = quantUse;
        this.itemName = itemName;
        this.isActive = isActive;
        this.subItemId = subItemId;
        this.mItemId = mItemId;
        this.isImage = isImage;
    }

    public List<quanListClass> getQuantUse() {
        return quantUse;
    }

    public String getItemName() {
        return itemName;
    }

    public int getIsActive() {
        return isActive;
    }

    public int getSubItemId() {
        return subItemId;
    }

    public int getmItemId() {
        return mItemId;
    }

    public int getIsImage() {
        return isImage;
    }
}
