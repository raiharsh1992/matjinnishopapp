package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class viewAdapterOrderDetailsItem extends RecyclerView.Adapter<viewAdapterOrderDetailsItem.viewHolderOrderDetailsItem>{
    private Context mCtx;
    private List<viewClassOrderDetailsItem> itemInfo;

    public viewAdapterOrderDetailsItem(Context mCtx, List<viewClassOrderDetailsItem> itemInfo) {
        this.mCtx = mCtx;
        this.itemInfo = itemInfo;
    }

    @NonNull
    @Override
    public viewHolderOrderDetailsItem onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.order_details_item_view,null);
        return new viewHolderOrderDetailsItem (view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderOrderDetailsItem holder, int position) {
        viewClassOrderDetailsItem container = itemInfo.get (position);
        if(container.getDisplayAddOn ()==0){
            holder.getDisplayAddOn.setVisibility (View.GONE);
        }
        else{
            holder.addOnName.setText (container.getAddOnName ());
        }
        if(container.getDisplaySubs ()==0){
            holder.displaySubs.setVisibility (View.GONE);
        }
        else{
            holder.subsStartDate.setText (container.getSusbStartDate ());
            holder.subsDelTime.setText (container.getSubsDelTime ());
            holder.subsDaysLeft.setText (container.getSubsDaysLeft ());
        }
        holder.itemName.setText (container.getItemName ());
        holder.quantity.setText (container.getQuantity ());
        holder.itemCost.setText (container.getTotalCost ());
    }

    @Override
    public int getItemCount() {
        return itemInfo.size ();
    }

    class viewHolderOrderDetailsItem extends RecyclerView.ViewHolder{
        TextView itemName, addOnName, quantity, itemCost, subsStartDate, subsDelTime, subsDaysLeft;
        LinearLayout displaySubs, getDisplayAddOn;
        public viewHolderOrderDetailsItem(View itemView){
            super(itemView);
            itemName = itemView.findViewById (R.id.itemName);
            addOnName = itemView.findViewById (R.id.addOnInfo);
            getDisplayAddOn = itemView.findViewById (R.id.displayAddOnInfo);
            quantity = itemView.findViewById (R.id.quantity);
            displaySubs = itemView.findViewById (R.id.displaySubsInfo);
            subsStartDate = itemView.findViewById (R.id.subsStartDate);
            subsDelTime = itemView.findViewById (R.id.subsDelTime);
            subsDaysLeft = itemView.findViewById (R.id.subsDaysLeft);
            itemCost = itemView.findViewById (R.id.itemCost);
        }
    }
}
