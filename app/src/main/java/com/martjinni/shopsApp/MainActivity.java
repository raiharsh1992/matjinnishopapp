package com.martjinni.shopsApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.martjinni.Appshops.R;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.contains("sessionInfo")){
            Intent intent = new Intent(this, orders.class);
            startActivity(intent);
            finish();
        }
        else{
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
    }
}
