package com.martjinni.shopsApp;

import java.util.List;

public class addMasterDispClass {
    private String masterName;
    private int masterId, rate, isMulti, isActive;
    private List<addSubDispClass> useAdd;

    public addMasterDispClass(String masterName, int masterId, int rate, int isMulti, int isActive, List<addSubDispClass> useAdd) {
        this.masterName = masterName;
        this.masterId = masterId;
        this.rate = rate;
        this.isMulti = isMulti;
        this.isActive = isActive;
        this.useAdd = useAdd;
    }

    public String getMasterName() {
        return masterName;
    }

    public int getMasterId() {
        return masterId;
    }

    public int getRate() {
        return rate;
    }

    public int getIsMulti() {
        return isMulti;
    }

    public int getIsActive() {
        return isActive;
    }

    public List<addSubDispClass> getUseAdd() {
        return useAdd;
    }
}
