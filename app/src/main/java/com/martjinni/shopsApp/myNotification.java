package com.martjinni.shopsApp;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.martjinni.Appshops.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND;
import static android.app.ActivityManager.RunningAppProcessInfo.IMPORTANCE_VISIBLE;
import static android.support.constraint.Constraints.TAG;

public class myNotification extends FirebaseMessagingService {
    int notificationID = 0;
    volleyClass newClass = new volleyClass ();
    private NotificationManager notifManager;
    @Override
    public void onNewToken(String token) {
        try{
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if(!sharedPreferences.getString ("sessionInfo","").equals ("")){
                sharedPreferences.edit ().putString ("userMessageToken",token).apply ();
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("token",token);
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"setusertoken";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            else{
                sharedPreferences.edit ().putString ("userMessageToken",token).apply ();
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }
    @SuppressWarnings("InflateParams")
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try{
            Log.i(TAG,String.valueOf (remoteMessage.getData ()));
            JSONObject palyloadData = new JSONObject (remoteMessage.getData ());
            String header = palyloadData.getString ("title");
            String body = palyloadData.getString ("body");
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                int importance = NotificationManager.IMPORTANCE_HIGH;
                if (notifManager == null) {
                    notifManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
                }
                NotificationChannel mChannel = notifManager.getNotificationChannel(String.valueOf (notificationID));
                if (mChannel == null) {
                    mChannel = new NotificationChannel (String.valueOf (notificationID), header, importance);
                    mChannel.enableVibration(true);
                    mChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                    notifManager.createNotificationChannel(mChannel);
                }
                Intent intent;
                PendingIntent pendingIntent;
                NotificationCompat.Builder builder;
                builder = new NotificationCompat.Builder(this, String.valueOf (notificationID));
                intent = new Intent(this, MainActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
                builder.setContentTitle(header)  // required
                        .setSmallIcon(R.drawable.ic_stat_name) // required
                        .setContentText(body)  // required
                        .setDefaults(Notification.DEFAULT_ALL)
                        .setAutoCancel(true)
                        .setContentIntent(pendingIntent)
                        .setTicker(header)
                        .setVibrate(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400})
                        .setSound (Uri.parse ("android.resource://"
                                + this.getPackageName() + "/" + R.raw.siren))
                        .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.use_icon));
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(notificationID, builder.build());
            }
            else{
                NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this,String.valueOf (notificationID));
                mBuilder.setSmallIcon(R.drawable.ic_stat_name);
                mBuilder.setColor(ContextCompat.getColor(getApplicationContext (), R.color.colorPrimary));
                mBuilder.setContentTitle(header);
                mBuilder.setContentText(body);
                mBuilder.setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });
                mBuilder.setSound (Uri.parse ("android.resource://"
                        + this.getPackageName() + "/" + R.raw.siren));
                mBuilder.setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.use_icon));
                Intent resultIntent = new Intent (this, orders.class);
                TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
                stackBuilder.addParentStack(orders.class);
                stackBuilder.addNextIntent(resultIntent);
                PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder.setContentIntent(resultPendingIntent);
                NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                mNotificationManager.notify(notificationID, mBuilder.build());
            }
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    public boolean foregrounded() {
        ActivityManager.RunningAppProcessInfo appProcessInfo = new ActivityManager.RunningAppProcessInfo();
        ActivityManager.getMyMemoryState(appProcessInfo);
        return (appProcessInfo.importance == IMPORTANCE_FOREGROUND || appProcessInfo.importance == IMPORTANCE_VISIBLE);
    }
}
