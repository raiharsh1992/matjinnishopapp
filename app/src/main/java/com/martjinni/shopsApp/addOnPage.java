package com.martjinni.shopsApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class addOnPage extends AppCompatActivity {
    RecyclerView displayMaster;
    volleyClass newClass = new volleyClass ();
    List<addMasterDispClass> userInfo = new ArrayList<> ();
    public volatile int workingMaster;
    public volatile boolean workingON = false;
    Context mCtx;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_add_on_page);
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        displayMaster = findViewById (R.id.displayAddMasterList);
        displayMaster.setHasFixedSize (false);
        displayMaster.setNestedScrollingEnabled (false);
        displayMaster.setLayoutManager (new LinearLayoutManager (this));
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            generateAddOnList runnable = new generateAddOnList ();
            mCtx = this;
            workingMaster = 0;
            new Thread (runnable).start ();
        }
    }

    class generateAddOnList implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"shopaddoninfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                userInfo = new ArrayList<> ();
                JSONArray dataSet = response.getJSONArray ("data");
                if(dataSet.length ()>0){
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject masterInfo = dataSet.getJSONObject (i);
                        JSONArray subInfo = masterInfo.getJSONArray ("itemInfo");
                        List<addSubDispClass> subDataSet = new ArrayList<> ();
                        for(int j=0;j<subInfo.length ();j++){
                            JSONObject interimSubData = subInfo.getJSONObject (j);
                            subDataSet.add (new addSubDispClass (interimSubData.getString ("addOnItemName"),interimSubData.getInt ("isActiveFl"),interimSubData.getInt ("addOnItemId"),masterInfo.getInt ("groupId")));
                        }
                        userInfo.add (new addMasterDispClass (masterInfo.getString ("groupName"),masterInfo.getInt ("groupId"),masterInfo.getInt("groupCharge"),masterInfo.getInt ("isMultiSelect"),masterInfo.getInt ("isActiveFl"),subDataSet));
                    }
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            addMasterDispAdapter adapter = new addMasterDispAdapter (mCtx, userInfo, workingMaster);
                            updateList (adapter);
                        }
                    });
                }
                else{
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            Toast.makeText (getApplicationContext (), "No addon yet, kindly start adding", Toast.LENGTH_LONG).show ();
                        }
                    });
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void addNewItem(final int masterId){
        if(workingON){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
            final View mView = getLayoutInflater().inflate(R.layout.add_new_sub_add_on, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            dialog.setCanceledOnTouchOutside(false);
            TextView continueButton = mView.findViewById (R.id.addButton);
            continueButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    EditText addOnName = mView.findViewById (R.id.addonname);
                    String addOnValue = addOnName.getText ().toString ();
                    if(addOnValue.equals ("")){
                        Toast.makeText (getApplicationContext (), "Kindly pass on a name", Toast.LENGTH_SHORT).show ();
                    }
                    else{
                        if(workingON){
                            insertIntoMaster runnable = new insertIntoMaster (dialog,masterId,addOnValue,addOnName);
                            new Thread (runnable).start ();
                            workingON = false;
                        }
                    }
                }
            });
            TextView cancelButton = mView.findViewById (R.id.cancelButton);
            cancelButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    workingON = true;
                    dialog.dismiss ();
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Already a transaction happening", Toast.LENGTH_SHORT).show ();
        }
    }

    class insertIntoMaster implements Runnable{
        AlertDialog dialog;
        int masterId;
        String addOnName;
        EditText addOnText;

        public insertIntoMaster(AlertDialog dialog, int masterId, String addOnName, EditText addOnText) {
            this.dialog = dialog;
            this.masterId = masterId;
            this.addOnName = addOnName;
            this.addOnText = addOnText;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("addOnId",masterId);
                requestObject.put ("addOnItemName",addOnName);
                String finalUrl = newClass.baseUrl+"iteminsertaddon";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generateAddOnList runner = new generateAddOnList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                addOnText.setError ("The name is already taken");
                                addOnText.findFocus ();
                                workingON = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void expandView(int masterId){
        if(workingON){
            if(masterId==workingMaster){
                workingMaster = 0;
                addMasterDispAdapter adapter = new addMasterDispAdapter (mCtx, userInfo, workingMaster);
                updateList (adapter);
            }
            else{
                workingMaster = masterId;
                addMasterDispAdapter adapter = new addMasterDispAdapter (mCtx, userInfo, workingMaster);
                updateList (adapter);
            }
        }
        else{
            Toast.makeText (getApplicationContext (), "Already a transaction happening", Toast.LENGTH_SHORT).show ();
        }
    }

    public void goBackToSettings(View view){
        goToOrders();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, shopSettings.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }

    public void updateList(addMasterDispAdapter adapter){
        displayMaster.setAdapter (adapter);
        workingON = true;
    }

    public void createNewAddOn(View view){
        if(workingON) {
            AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
            final View mView = getLayoutInflater ().inflate (R.layout.add_new_master_add_on, null);
            mBuilder.setView (mView);
            final AlertDialog dialog = mBuilder.create ();
            dialog.setCanceledOnTouchOutside (false);
            TextView continueUse = mView.findViewById (R.id.continueAdd);
            TextView cancelUse = mView.findViewById (R.id.cancelButton);
            cancelUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    workingON = true;
                    dialog.dismiss ();
                }
            });
            continueUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    EditText masterName = mView.findViewById (R.id.addonname);
                    EditText masterPriceText = mView.findViewById (R.id.addoncost);
                    Switch isMulti = mView.findViewById (R.id.switch1);
                    String masterNameValue = masterName.getText ().toString ();
                    String masterPriceValue = masterPriceText.getText ().toString ();
                    if(masterNameValue.equals ("")){
                        masterName.setError ("Kindly pass a add-on name");
                        masterName.findFocus ();
                    }
                    else{
                        if(masterPriceValue.equals ("")){
                            masterPriceText.setError ("Kindly pass a value");
                            masterPriceText.findFocus ();
                        }
                        else{
                            int isMultiValue = 0;
                            if(isMulti.isChecked ()){
                                isMultiValue = 1;
                            }
                            if(workingON){
                                insertNewMaster running = new insertNewMaster (masterNameValue,isMultiValue,Integer.parseInt (masterPriceValue),dialog, masterName);
                                new Thread (running).start ();
                                workingON = false;
                            }
                        }
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Already working on something", Toast.LENGTH_LONG).show ();
        }
    }

    class insertNewMaster implements Runnable{
        String addOnName;
        int isMulti, cost;
        AlertDialog dialog;
        EditText masterName;

        public insertNewMaster(String addOnName, int isMulti, int cost, AlertDialog dialog, EditText masterName) {
            this.addOnName = addOnName;
            this.isMulti = isMulti;
            this.cost = cost;
            this.dialog = dialog;
            this.masterName = masterName;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("addOnCharge",cost);
                requestObject.put ("addOnName",addOnName);
                requestObject.put ("isMultiSelect",isMulti);
                String finalUrl = newClass.baseUrl+"insertaddon";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generateAddOnList runner = new generateAddOnList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                masterName.setError ("The name is already taken");
                                masterName.findFocus ();
                                workingON = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void groupEdit(int position){
        if(workingON){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
            final View mView = getLayoutInflater ().inflate (R.layout.add_on_group_edit, null);
            mBuilder.setView (mView);
            final AlertDialog dialog = mBuilder.create ();
            dialog.setCanceledOnTouchOutside (false);
            TextView continueUse = mView.findViewById (R.id.continueAdd);
            TextView cancelUse = mView.findViewById (R.id.cancelButton);
            TextView addOnName = mView.findViewById (R.id.addOnName);
            final EditText addoncost = mView.findViewById (R.id.addoncost);
            final Switch addOnStatus = mView.findViewById (R.id.switchonoff);
            final Switch switchmultiselect = mView.findViewById (R.id.switchmultiselect);
            final addMasterDispClass container = userInfo.get (position);
            addOnName.setText (container.getMasterName ());
            if(container.getIsActive ()==1){
                addOnStatus.setChecked (true);
            }
            else{
                addOnStatus.setChecked (false);
            }
            if(container.getIsMulti ()==1){
                switchmultiselect.setChecked (true);
            }
            else{
                switchmultiselect.setChecked (false);
            }
            addoncost.setText (String .valueOf (container.getRate ()));
            cancelUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    workingON = true;
                }
            });
            continueUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    String newCost = addoncost.getText ().toString ();
                    if(newCost.equals ("")){
                        Toast.makeText (getApplicationContext (), "Kindly pass some cost", Toast.LENGTH_LONG).show ();
                    }
                    else{
                        int isMulti = 0, isActive = 0, cost;
                        cost = Integer.parseInt (newCost);
                        if(addOnStatus.isChecked ()){
                            isActive = 1;
                        }
                        if(switchmultiselect.isChecked ()){
                            isMulti = 1;
                        }
                        updateGroupInfo running = new updateGroupInfo(cost, isActive, isMulti, container.getMasterName (),dialog);
                        new Thread (running).start ();
                        workingON = false;
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Already working on something", Toast.LENGTH_LONG).show ();
        }
    }

    class updateGroupInfo implements Runnable{
        int cost, isActive, isMulti;
        String addOnName;
        AlertDialog dialog;

        public updateGroupInfo(int cost, int isActive, int isMulti, String addOnName, AlertDialog dialog) {
            this.cost = cost;
            this.isActive = isActive;
            this.isMulti = isMulti;
            this.addOnName = addOnName;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("addOnCharge",cost);
                requestObject.put ("addOnName",addOnName);
                requestObject.put ("isMultiSelect",isMulti);
                requestObject.put ("isActiveFl",isActive);
                String finalUrl = newClass.baseUrl+"updateaddon";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generateAddOnList runner = new generateAddOnList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                workingON = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void addOnItem(final int subId, int isActive, final String itemName){
        if(workingON){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
            final View mView = getLayoutInflater ().inflate (R.layout.add_on_sub_edit, null);
            mBuilder.setView (mView);
            final AlertDialog dialog = mBuilder.create ();
            dialog.setCanceledOnTouchOutside (false);
            TextView addOnName = mView.findViewById (R.id.addOnName);
            final Switch itemStatus = mView.findViewById (R.id.switch1);
            TextView continueUse = mView.findViewById (R.id.continueAdd);
            TextView cancelUse = mView.findViewById (R.id.cancelButton);
            addOnName.setText (itemName);
            if(isActive==0){
                itemStatus.setChecked (false);
            }
            else{
                itemStatus.setChecked (true);
            }
            cancelUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    workingON = true;
                    dialog.dismiss ();
                }
            });
            continueUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int updatedIsActive = 0;
                    if(itemStatus.isChecked ()){
                        updatedIsActive = 1;
                    }
                    updateGroupItem fastrunner = new updateGroupItem (itemName, updatedIsActive, subId, dialog);
                    new Thread (fastrunner).start ();
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Already working on something", Toast.LENGTH_LONG).show ();
        }
    }

    class updateGroupItem implements Runnable{
        String itemName;
        int isActive, subId;
        AlertDialog dialog;

        public updateGroupItem(String itemName, int isActive, int subId, AlertDialog dialog) {
            this.itemName = itemName;
            this.isActive = isActive;
            this.subId = subId;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("addOnItemName",itemName);
                requestObject.put ("addOnId",subId);
                requestObject.put ("isActiveFl",isActive);
                Log.i ("Request",String.valueOf (requestObject));
                String finalUrl = newClass.baseUrl+"itemupdateaddon";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generateAddOnList runner = new generateAddOnList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), itemName+" add-on successfully updated", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                workingON = true;
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Issue while updating "+itemName+" add-on", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }
}
