package com.martjinni.shopsApp;

import java.util.List;

public class eachMenuMainClass {
    private String mainItemName;
    private int mainItemId, isActive;
    private List<eachMenuItemClass> userInfo;

    public eachMenuMainClass(String mainItemName, int mainItemId, int isActive, List<eachMenuItemClass> userInfo) {
        this.mainItemName = mainItemName;
        this.mainItemId = mainItemId;
        this.isActive = isActive;
        this.userInfo = userInfo;
    }

    public String getMainItemName() {
        return mainItemName;
    }

    public int getMainItemId() {
        return mainItemId;
    }

    public int getIsActive() {
        return isActive;
    }

    public List<eachMenuItemClass> getUserInfo() {
        return userInfo;
    }
}
