package com.martjinni.shopsApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.martjinni.Appshops.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_getotp extends Fragment {

    EditText otpval;
    TextView action;
    TextView resend;
    public fragment_getotp() {
        // Required empty public constructor


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_restore_getotp, container, false);

        action = view.findViewById(R.id.finalregister);
        resend = view.findViewById(R.id.resendotp);
        otpval = view.findViewById(R.id.getotp);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otpvalnew = otpval.getText().toString();
                if(otpvalnew.length() <6 || otpvalnew.length() >6)
                {
                    Toast.makeText(getContext(),"Otp must be of 6 Digits", Toast.LENGTH_LONG).show();
                }
                else{
                    ((changepassword)getActivity()).validateotp(otpvalnew);
                }

            }
        });
        resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((changepassword)getActivity()).generateOtp();
            }
        });
        return view;
    }

}
