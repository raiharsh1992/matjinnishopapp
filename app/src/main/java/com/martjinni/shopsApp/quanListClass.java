package com.martjinni.shopsApp;

public class quanListClass {
    private String quantName;
    private int mrp, valueUse, isActive, quantId;

    public quanListClass(String quantName, int mrp, int valueUse, int isActive, int quantId) {
        this.quantName = quantName;
        this.mrp = mrp;
        this.valueUse = valueUse;
        this.isActive = isActive;
        this.quantId = quantId;
    }

    public String getQuantName() {
        return quantName;
    }

    public int getMrp() {
        return mrp;
    }

    public int getValueUse() {
        return valueUse;
    }

    public int getIsActive() {
        return isActive;
    }

    public int getQuantId() {
        return quantId;
    }

    public void setQuantName(String quantName) {
        this.quantName = quantName;
    }

    public void setMrp(int mrp) {
        this.mrp = mrp;
    }

    public void setValueUse(int valueUse) {
        this.valueUse = valueUse;
    }
}
