package com.martjinni.shopsApp;

public class addSubDispClass {
    private String subName;
    private int subId;
    private int masterId;
    private int subStatus;

    public addSubDispClass(String subName, int subStatus, int subId, int masterId) {
        this.subName = subName;
        this.subStatus = subStatus;
        this.subId = subId;
        this.masterId = masterId;
    }


    public String getSubName() {
        return subName;
    }

    public int getSubStatus() {
        return subStatus;
    }

    public int getSubId() {
        return subId;
    }

    public int getMasterId() {
        return masterId;
    }
}
