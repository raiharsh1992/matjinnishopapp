package com.martjinni.shopsApp;

public class delBoyListClass {
    private String delBoyName, delPhone;
    private int delBoyId;

    public delBoyListClass(String delBoyName, String delPhone, int delBoyId) {
        this.delBoyName = delBoyName;
        this.delPhone = delPhone;
        this.delBoyId = delBoyId;
    }

    public String getDelBoyName() {
        return delBoyName;
    }

    public String getDelPhone() {
        return delPhone;
    }

    public int getDelBoyId() {
        return delBoyId;
    }
}
