package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class eachMenuItemAdapter extends RecyclerView.Adapter<eachMenuItemAdapter.eachMenuItemViewHolder>{

    Context mCtx;
    List<eachMenuItemClass> useInfo;

    public eachMenuItemAdapter(Context mCtx, List<eachMenuItemClass> useInfo) {
        this.mCtx = mCtx;
        this.useInfo = useInfo;
    }

    @NonNull
    @Override
    public eachMenuItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.item_menu_each,null);
        return new eachMenuItemViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull eachMenuItemViewHolder holder, int position) {
        eachMenuItemClass container = useInfo.get (position);
        holder.itemName.setText (container.getItemName ());
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class eachMenuItemViewHolder extends RecyclerView.ViewHolder{
        TextView itemName;
        LinearLayout editItem, manageImage, manageListing;
        public eachMenuItemViewHolder(View itemView){
            super(itemView);
            itemName = itemView.findViewById (R.id.subItemName);
            editItem = itemView.findViewById (R.id.editItem);
            editItem.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    eachMenuItemClass container = useInfo.get (position);
                    ((itemsPage)mCtx).updateSubItemName (container.getIsActive (),container.getItemName (),container.getmItemId (),container.getSubItemId ());
                }
            });
            manageImage = itemView.findViewById (R.id.imageSet);
            manageImage.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    eachMenuItemClass container = useInfo.get (position);
                    ((itemsPage)mCtx).updateImage (container);
                }
            });
            manageListing = itemView.findViewById (R.id.manageListing);
            manageListing.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    eachMenuItemClass container = useInfo.get (position);
                    if(container.getQuantUse ().size ()>0){
                        ((itemsPage)mCtx).displayQuant(container.getQuantUse (),container.getSubItemId (),container.getmItemId (),container.getItemName ());
                    }
                    else{
                        ((itemsPage)mCtx).addNewQuant(container.getSubItemId (),container.getmItemId ());
                    }
                }
            });
        }
    }
}
