package com.martjinni.shopsApp;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class promoListAdapter extends RecyclerView.Adapter<promoListAdapter.promoListViewHolder>{

    Context mCtx;
    List<promoLIstClass> useAdd;

    public promoListAdapter(Context mCtx, List<promoLIstClass> useAdd) {
        this.mCtx = mCtx;
        this.useAdd = useAdd;
    }

    @NonNull
    @Override
    public promoListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.promo_list,null);
        return new promoListViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull promoListViewHolder holder, int position) {
        promoLIstClass container = useAdd.get (position);
        if(container.getPromoStatus ()==0){
            holder.isActie.setChecked (false);
            holder.promoStatus.setText ("IN-ACTIVE");
            holder.promoStatus.setTextColor (Color.parseColor ("#bad70303"));
        }
        else{
            holder.isActie.setChecked (true);
            holder.promoStatus.setText ("ACTIVE");
            holder.promoStatus.setTextColor (Color.parseColor ("#ba7ed321"));
        }
        if(container.getPromoTYpe ().equals ("P")){
            String disCount = container.getPromoDiscount ()+"%";
            holder.discountAmount.setText (disCount);
            holder.promoType.setText ("PERCENTAGE");
        }
        else{
            String disCount ="Rs "+ container.getPromoDiscount ();
            holder.discountAmount.setText (disCount);
            holder.promoType.setText ("Amount");
        }
        if(container.getIsMultiUse ()==1){
            holder.isMultiUse.setText ("YES");
            holder.isMultiUse.setTextColor (Color.parseColor ("#ba7ed321"));
        }
        else{
            holder.isMultiUse.setText ("NO");
            holder.isMultiUse.setTextColor (Color.parseColor ("#bad70303"));
        }
        holder.promoName.setText (container.getPromoName ());
        holder.createdOn.setText (container.getCreatedOn ());
        holder.minAmount.setText (container.getMinAmount ());
    }

    @Override
    public int getItemCount() {
        return useAdd.size ();
    }

    class promoListViewHolder extends RecyclerView.ViewHolder{
        TextView promoName, createdOn, isMultiUse, promoStatus, minAmount, discountAmount, promoType;
        Switch isActie;
        public promoListViewHolder(View itemView){
            super(itemView);
            promoName = itemView.findViewById (R.id.promoName);
            isActie = itemView.findViewById (R.id.switchStatus);
            createdOn = itemView.findViewById (R.id.createdOn);
            isMultiUse = itemView.findViewById (R.id.isMultiUse);
            promoStatus = itemView.findViewById (R.id.promoStatus);
            minAmount = itemView.findViewById (R.id.minAmount);
            discountAmount = itemView.findViewById (R.id.promoDiscount);
            promoType = itemView.findViewById (R.id.promoType);
            isActie.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    promoLIstClass container = useAdd.get (position);
                    int newStatus = 0;
                    if(container.getPromoStatus ()==0){
                        newStatus = 1;
                    }
                    ((promoCode)mCtx).updatePromoValue (isActie, newStatus, container.getPromoId ());
                }
            });
        }
    }
}
