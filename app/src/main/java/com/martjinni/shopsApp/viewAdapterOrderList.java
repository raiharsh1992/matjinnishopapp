package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class viewAdapterOrderList extends RecyclerView.Adapter<viewAdapterOrderList.viewHolderOrderList>{

    private Context mCtx;
    private List<viewClassOrderList> infoUsed;

    public viewAdapterOrderList(Context mCtx, List<viewClassOrderList> infoUsed) {
        this.mCtx = mCtx;
        this.infoUsed = infoUsed;
    }

    @NonNull
    @Override
    public viewHolderOrderList onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.order_list_layout,null);
        return new viewHolderOrderList (view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolderOrderList holder, int position) {
        viewClassOrderList container = infoUsed.get (position);
        holder.custName.setText (container.getCustomerName ());
        holder.totalAmount.setText (container.getTotalAmount ());
        holder.paymentMethod.setText (container.getPayMethod ());
        holder.orderStatus.setText (container.getOrderStatus ());
        String orderId = "Order Id : "+container.getOrderId ();
        holder.orderId.setText (orderId);
        if(container.getDisplayAssign ()>0){
            holder.assignDm.setVisibility (View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return infoUsed.size ();
    }


    class viewHolderOrderList extends RecyclerView.ViewHolder{
        TextView custName, orderId, totalAmount, orderStatus, paymentMethod, viewDetails, assignDm;
        public viewHolderOrderList(View itemView){
            super(itemView);
            custName = itemView.findViewById (R.id.customerName);
            totalAmount = itemView.findViewById (R.id.totalAmount);
            orderStatus = itemView.findViewById (R.id.orderStatus);
            paymentMethod = itemView.findViewById (R.id.paymentMethod);
            viewDetails = itemView.findViewById (R.id.orderDetails);
            assignDm = itemView.findViewById (R.id.assignOrder);
            orderId = itemView.findViewById (R.id.orderId);
            assignDm.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    viewClassOrderList container = infoUsed.get (position);
                    ((orders)mCtx).assignDeliveryBoy (container.getOrderId ());
                }
            });
            viewDetails.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    viewClassOrderList container = infoUsed.get (position);
                    ((orders)mCtx).goToNext (container.getOrderId ());
                }
            });
        }
    }
}
