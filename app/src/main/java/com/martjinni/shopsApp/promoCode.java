package com.martjinni.shopsApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class promoCode extends AppCompatActivity {
    volleyClass newClass = new volleyClass ();
    RecyclerView displayPromoList;
    Context mCtx;
    List<promoLIstClass> useAdd = new ArrayList<> ();
    public volatile  boolean isStateChange = false;
    public volatile boolean isCreatingNew = false;
    int currentSelected = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_promo_code);
        displayPromoList = findViewById (R.id.displayPromo);
        displayPromoList.setHasFixedSize (false);
        displayPromoList.setNestedScrollingEnabled (false);
        displayPromoList.setLayoutManager (new LinearLayoutManager (this));
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            mCtx = this;
            generatingPromoList runnable = new generatingPromoList ();
            new Thread (runnable).start ();
        }
    }

    class generatingPromoList implements Runnable{

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                String finalUrl = newClass.baseUrl+"getpromolist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                useAdd = new ArrayList<> ();
                if(response.getInt ("count")>0){
                    JSONArray dataSet = response.getJSONArray ("Data");
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject useData = dataSet.getJSONObject (i);
                        useAdd.add (new promoLIstClass (useData.getString ("promoCode"),useData.getString ("createdOn"),useData.getInt ("isMulti"),useData.getInt ("isActive"),String.valueOf (useData.getInt ("minAmount")),String.valueOf (useData.getInt ("disAmount")),useData.getString ("promoType"),useData.getInt ("promoId")));
                    }
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            promoListAdapter adapter = new promoListAdapter (mCtx,useAdd);
                            displayPromoList.setAdapter (adapter);
                            isStateChange = true;
                            isCreatingNew = true;
                        }
                    });
                }
                else{
                    isStateChange = true;
                    isCreatingNew = true;
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            Toast.makeText (getApplicationContext (), "No promo's added yet, start by adding", Toast.LENGTH_LONG).show ();
                        }
                    });
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void backPressed(View view){
        goToOrders ();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, shopSettings.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }

    public void updatePromoValue(Switch workingUnit, int newStatus, int promoId){
        if(isStateChange&&isCreatingNew){
            updatePromoCode runner = new updatePromoCode (newStatus, promoId);
            new Thread (runner).start ();
            isStateChange = false;
        }
        else{
            workingUnit.toggle ();
            Toast.makeText (getApplicationContext (), "Already a request is being processed", Toast.LENGTH_LONG).show ();
        }
    }

    class updatePromoCode implements Runnable{
        int newStatus, promoId;

        public updatePromoCode(int newStatus, int promoId) {
            this.newStatus = newStatus;
            this.promoId = promoId;
        }

        @Override
        public  void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("updateValue",newStatus);
                requestObject.put ("promoId",promoId);
                String finalUrl = newClass.baseUrl+"updatepromo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generatingPromoList runnable = new generatingPromoList ();
                        new Thread (runnable).start ();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText (getApplicationContext (), "Max active promo code already present", Toast.LENGTH_LONG).show ();
                        generatingPromoList runnable = new generatingPromoList ();
                        new Thread (runnable).start ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void createNewPromo(View view){
        if(isStateChange&&isCreatingNew){
            currentSelected = 0;
            isCreatingNew = false;
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put("propertyName","maxPromoCount");
                String finalUrl = newClass.baseUrl+"validateshopprop";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        workForCreation();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText (getApplicationContext (), "Max number of active promo-code already present", Toast.LENGTH_SHORT).show ();
                        isCreatingNew = true;
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
        else{
            Toast.makeText (getApplicationContext (), "Already a request is being processed", Toast.LENGTH_LONG).show ();
        }
    }

    public void workForCreation(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        final View mView = getLayoutInflater().inflate(R.layout.create_new_promocode, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.setCanceledOnTouchOutside(false);
        TextView cancelAction = mView.findViewById (R.id.cancel_action);
        cancelAction.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                isCreatingNew = true;
            }
        });
        final CheckBox percenTage = mView.findViewById (R.id.percent);
        percenTage.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if(currentSelected==0){
                    currentSelected = 1;
                }
                else if(currentSelected==1){
                    currentSelected = 0;
                    percenTage.setChecked (false);
                }
                else{
                    currentSelected = 1;
                    CheckBox amount1 = mView.findViewById (R.id.admount);
                    amount1.setChecked (false);
                }
            }
        });
        final CheckBox amount = mView.findViewById (R.id.admount);
        amount.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if(currentSelected==0){
                    currentSelected = 2;
                }
                else if(currentSelected==2){
                    currentSelected = 0;
                    amount.setChecked (false);
                }
                else{
                    currentSelected = 2;
                    CheckBox amount1 = mView.findViewById (R.id.percent);
                    amount1.setChecked (false);
                }
            }
        });
        TextView addNewPromo = mView.findViewById (R.id.addNewPromo);
        addNewPromo.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                EditText promoName = mView.findViewById (R.id.promocodename);
                EditText promoDiscount = mView.findViewById (R.id.promoamt);
                EditText minAMount = mView.findViewById (R.id.minamt);
                Switch isMulti = mView.findViewById (R.id.switch1);
                String promoNameText = promoName.getText ().toString ();
                if(promoNameText.equals ("")){
                    Toast.makeText (getApplicationContext (), "Kindly pass promo name", Toast.LENGTH_SHORT).show ();
                }
                else{
                    String discountAmountText = (promoDiscount.getText ().toString ());
                    if(discountAmountText.equals ("")){
                        Toast.makeText (getApplicationContext (), "Kindly pass a promo value", Toast.LENGTH_SHORT).show ();
                    }
                    else{
                        int discountAmount = Integer.parseInt (discountAmountText);
                        if(currentSelected==0){
                            Toast.makeText (getApplicationContext (), "Kindly select a promo type", Toast.LENGTH_SHORT).show ();
                        }
                        else{
                            int minAmount = 0;
                            String info = minAMount.getText ().toString ();
                            if(!info.equals ("")){
                                minAmount = Integer.parseInt (String.valueOf (minAMount.getText ()));
                                if(minAmount>0){
                                    String promoTypeUse = "A";
                                    if(currentSelected==1){
                                        promoTypeUse = "P";
                                    }
                                    int isMultiUse = 0;
                                    if(isMulti.isChecked ()){
                                        isMultiUse = 1;
                                    }
                                    insertNewPromoCode runner = new insertNewPromoCode (promoNameText, minAmount,discountAmount,isMultiUse,promoTypeUse,dialog,promoName );
                                    new Thread (runner).start ();
                                }
                                else{
                                    Toast.makeText (getApplicationContext (), "Kindly pass minimum amount greater than 0", Toast.LENGTH_SHORT).show ();
                                }
                            }
                        }
                    }
                }
            }
        });
        dialog.show ();
    }

    class insertNewPromoCode implements Runnable{
        String promoName, promoTypeUse;
        int minValue, disCount, isMulti;
        AlertDialog dialog;
        EditText promoCodeName;

        public insertNewPromoCode(String promoName, int minValue, int disCount, int isMulti, String promoType, AlertDialog dialog, EditText promoCodeName) {
            this.promoName = promoName;
            this.minValue = minValue;
            this.disCount = disCount;
            this.isMulti = isMulti;
            this.promoTypeUse = promoType;
            this.dialog = dialog;
            this.promoCodeName = promoCodeName;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("promoCode",promoName);
                String finalUrl = newClass.baseUrl+"isnewpromo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        insertNewPromo();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        promoCodeName.setError ("The name is already taken");
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void insertNewPromo(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("promoAmount",disCount);
                requestObject.put ("minValue",minValue);
                requestObject.put ("promoCode",promoName);
                requestObject.put ("promoType",promoTypeUse);
                requestObject.put ("isMulti",isMulti);
                String finalUrl = newClass.baseUrl+"insertpromo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generatingPromoList runnable = new generatingPromoList ();
                        new Thread (runnable).start ();
                        dialog.dismiss ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Promocode created", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        generatingPromoList runnable = new generatingPromoList ();
                        new Thread (runnable).start ();
                        dialog.dismiss ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Issue while creating promocode", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }
}
