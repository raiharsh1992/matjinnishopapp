package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class delBoyListAdapter extends RecyclerView.Adapter<delBoyListAdapter.delBoyViewHolder>{
    private Context mCtx;
    private List<delBoyListClass> useList;

    public delBoyListAdapter(Context mCtx, List<delBoyListClass> useList) {
        this.mCtx = mCtx;
        this.useList = useList;
    }

    @NonNull
    @Override
    public delBoyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.layout_delivery_boy_list,null);
        return new delBoyViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull delBoyViewHolder holder, int position) {
        delBoyListClass container = useList.get (position);
        holder.delBoyName.setText (container.getDelBoyName ());
        holder.delPhone.setText (container.getDelPhone ());
    }

    @Override
    public int getItemCount() {
        return useList.size ();
    }

    class delBoyViewHolder extends RecyclerView.ViewHolder{
        TextView delBoyName, delPhone;
        public delBoyViewHolder(View itemView){
            super(itemView);
            delBoyName = itemView.findViewById (R.id.delboyname);
            delPhone = itemView.findViewById (R.id.editphone);
        }
    }
}
