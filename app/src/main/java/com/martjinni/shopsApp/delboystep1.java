package com.martjinni.shopsApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.martjinni.Appshops.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class delboystep1 extends Fragment {

    TextView fragmentAction;
    EditText username;
    EditText pass1;
    EditText pass2;
    String un="";
    String p1="";
    String p2="";
    public RequestQueue mrequest;
    volleyClass newClass = new volleyClass();
    public String baseUrl = newClass.baseUrl;

    public delboystep1() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view =  inflater.inflate(R.layout.fragment_delboystep1, container, false);
        fragmentAction = view.findViewById(R.id.fragmentaction);
        username = view.findViewById(R.id.username);
        pass1 = view.findViewById(R.id.pass1);
        pass2 = view.findViewById(R.id.pass2);
        fragmentAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                un = username.getText().toString();
                p1 = pass1.getText().toString();
                p2 = pass2.getText().toString();

                if(p1.equals("") || p2.equals("") || un.equals("")){
                    ((adddelboy)getActivity()).alertshow("Please fill all the fields.");
                }
                else if(!p1.equals(p2)){
                    ((adddelboy)getActivity()).alertshow("Passwords Do not Match");
                }
                else if(p1.length()<8 || p1.length()>24){
                    ((adddelboy)getActivity()).alertshow("Password must be between 8 to 24 characters");
                }
                else if (un.length()< 4){
                    ((adddelboy)getActivity()).alertshow("Username must be greater than 4 characters");
                }
                else{
                    ((adddelboy)getActivity()).validateusername(un, p1);
                }
            }
        });
        return view;
    }

}
