package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class addMasterDispAdapter extends RecyclerView.Adapter<addMasterDispAdapter.addMasterDispViewHolder>{
    private Context mCtx;
    private List<addMasterDispClass> userInfo;
    private int workingState;

    public addMasterDispAdapter(Context mCtx, List<addMasterDispClass> userInfo, int workingState) {
        this.mCtx = mCtx;
        this.userInfo = userInfo;
        this.workingState = workingState;
    }

    @NonNull
    @Override
    public addMasterDispViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.addon_master_view,null);
        return new addMasterDispViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull addMasterDispViewHolder holder, int position) {
        addMasterDispClass container = userInfo.get (position);
        String masterName = container.getMasterName ()+" @ Rs. "+container.getRate ();
        holder.addMasterName.setText (masterName);
        holder.displaySubListView.setNestedScrollingEnabled (false);
        holder.displaySubListView.setHasFixedSize (false);
        holder.displaySubListView.setLayoutManager (new LinearLayoutManager (mCtx));
        addSubDispAdapter adapter = new addSubDispAdapter (mCtx,container.getUseAdd ());
        holder.displaySubListView.setAdapter (adapter);
        if(workingState==container.getMasterId ()){
            holder.displaySubListIf.setVisibility (View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return userInfo.size ();
    }

    class addMasterDispViewHolder extends RecyclerView.ViewHolder{
        TextView addMasterName;
        ImageView masterEdit;
        LinearLayout displaySubListIf;
        RecyclerView displaySubListView;
        FloatingActionButton addAddOn;
        public addMasterDispViewHolder(View itemView){
            super(itemView);
            addMasterName = itemView.findViewById (R.id.masterName);
            masterEdit = itemView.findViewById (R.id.masterEdit);
            displaySubListIf = itemView.findViewById (R.id.displaySubListIf);
            displaySubListView = itemView.findViewById (R.id.displaySubList);
            addAddOn = itemView.findViewById (R.id.addonadd);
            addAddOn.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    Log.i ("Wow","NEW ADD");
                    int position = getAdapterPosition ();
                    addMasterDispClass container = userInfo.get (position);
                    ((addOnPage)mCtx).addNewItem(container.getMasterId ());
                }
            });
            masterEdit.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    Log.i ("Wow","NEW ADD");
                    int position = getAdapterPosition ();
                    ((addOnPage)mCtx).groupEdit(position);
                }
            });
            addMasterName.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    addMasterDispClass container = userInfo.get (position);
                    ((addOnPage)mCtx).expandView(container.getMasterId ());
                }
            });
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    addMasterDispClass container = userInfo.get (position);
                    ((addOnPage)mCtx).expandView(container.getMasterId ());
                }
            });
        }
    }
}
