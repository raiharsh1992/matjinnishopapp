package com.martjinni.shopsApp;

public class promoLIstClass {
    private String promoName, createdOn,   minAmount, promoDiscount, promoTYpe;
    private int promoId, promoStatus,isMultiUse;

    public promoLIstClass(String promoName, String createdOn, int isMultiUse, int promoStatus, String minAmount, String promoDiscount, String promoTYpe, int promoId) {
        this.promoName = promoName;
        this.createdOn = createdOn;
        this.isMultiUse = isMultiUse;
        this.promoStatus = promoStatus;
        this.minAmount = minAmount;
        this.promoDiscount = promoDiscount;
        this.promoTYpe = promoTYpe;
        this.promoId = promoId;
    }

    public String getPromoName() {
        return promoName;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public int getIsMultiUse() {
        return isMultiUse;
    }

    public int getPromoStatus() {
        return promoStatus;
    }

    public String getMinAmount() {
        return minAmount;
    }

    public String getPromoDiscount() {
        return promoDiscount;
    }

    public String getPromoTYpe() {
        return promoTYpe;
    }

    public int getPromoId() {
        return promoId;
    }
}
