package com.martjinni.shopsApp;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class displayShopTImeAdapter extends RecyclerView.Adapter<displayShopTImeAdapter.displayShopTimeViewHolder>{

    Context mCtx;
    List<displayShopTImeClass> useInfo;
    RecyclerView mView;

    public displayShopTImeAdapter(Context mCtx, List<displayShopTImeClass> useInfo, RecyclerView mView) {
        this.mCtx = mCtx;
        this.useInfo = useInfo;
        this.mView = mView;
    }

    @NonNull
    @Override
    public displayShopTimeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.sub_time_list_card,null);
        return new displayShopTimeViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull displayShopTimeViewHolder holder, int position) {
        displayShopTImeClass container = useInfo.get (position);
        holder.timeValue.setText (container.getShopTime ());
        holder.timeDescription.setText (container.getTimeDesc ());
        holder.timeName.setText (container.getTimeName ());
        if(container.getIsActive ()==1){
            holder.setIsActive.setChecked (true);
            holder.timeStatus.setText ("ACTIVE");
            holder.timeStatus.setTextColor (Color.parseColor ("#ba7ed321"));
        }
        else{
            holder.setIsActive.setChecked (false);
            holder.timeStatus.setText ("IN-ACTIVE");
            holder.timeStatus.setTextColor (Color.parseColor ("#bad70303"));
        }
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class displayShopTimeViewHolder extends RecyclerView.ViewHolder{
        TextView timeName, timeDescription, timeValue, timeStatus;
        Switch setIsActive;
        public displayShopTimeViewHolder(View itemView){
            super(itemView);
            timeName = itemView.findViewById (R.id.itemquntname);
            timeDescription = itemView.findViewById (R.id.itemmrp);
            timeValue = itemView.findViewById (R.id.itemcost);
            timeStatus = itemView.findViewById (R.id.timeStatus);
            setIsActive = itemView.findViewById (R.id.onoff);
            setIsActive.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    displayShopTImeClass container = useInfo.get (position);
                    int isActive = 0;
                    if(container.getIsActive ()==0){
                        isActive = 1;
                    }
                    ((subscriptionView)mCtx).updateTimeStatus(container.getTimeId (),container.getShopTime (),container.getTimeName (),container.getTimeDesc (),isActive,setIsActive,mView);
                }
            });
        }
    }
}
