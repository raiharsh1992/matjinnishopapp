package com.martjinni.shopsApp;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class itemsPage extends AppCompatActivity {
    Context mCtx;
    volleyClass newClass = new volleyClass ();
    private static final int REQUEST_EXTERNAL_STORAGE = 1;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };
    public volatile boolean workingOnSomething = false;
    public volatile boolean isInitializedAccount = false;
    public volatile boolean isCatInitialized = false;
    public volatile boolean usingGst = false;
    public volatile boolean usingSub = false;
    public volatile boolean usingAddOn = false;
    public volatile boolean addingNewSub = true;
    public volatile boolean addingNewAddOn = true;
    public volatile int totalImages = 0;
    public volatile int totalSubQuant = 1;
    public volatile int allowedImages = 0;
    public volatile int workingMainItem = 0;
    public volatile int workingGst = 1;
    public volatile String addOnListItem = "";
    public volatile String subListItem = "";
    public volatile int selectedMaxQuant = 1;
    public static final int PICK_IMAGE = 1;
    public Uri itemUpload;
    RecyclerView menuList;
    List<eachMenuMainClass> useData = new ArrayList<> ();
    List<String> catInformtion = new ArrayList<> ();
    List<Integer> catId = new ArrayList<> ();
    List<String> gstInfo = new ArrayList<> ();
    List<Integer> gstId = new ArrayList<> ();
    List<addAddOnItemClass> addOnInformation = new ArrayList<> ();
    List<addAddOnItemClass> subInformation = new ArrayList<> ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_items_page);
        if (shouldAskPermissions()) {
            askPermissions();
        }
        mCtx = this;
        itemUpload = null;
        menuList = findViewById (R.id.mainMenu);
        menuList.setHasFixedSize (false);
        menuList.setNestedScrollingEnabled (false);
        menuList.setLayoutManager (new LinearLayoutManager (mCtx));
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            displayPrimartyList runner = new displayPrimartyList();
            new Thread (runner).start ();

            initializeAccount runnable = new initializeAccount();
            new Thread (runnable).start ();

            initializeInsertion running = new initializeInsertion();
            new Thread (running).start ();
        }
    }

    class initializeAccount implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"getshopproperty";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                JSONArray dataSet = response.getJSONArray ("data");
                for(int i=0;i<dataSet.length ();i++){
                    JSONObject interimObject = dataSet.getJSONObject (i);
                    if(interimObject.getString ("propertyName").equals ("maxImage")){
                        if(interimObject.getString ("propertyValue").equals ("")){
                            allowedImages = 0;
                        }
                        else {
                            allowedImages = Integer.parseInt (interimObject.getString ("propertyValue"));
                        }
                    }
                    else if(interimObject.getString ("propertyName").equals ("useSubQuantity")){
                        totalSubQuant = Integer.parseInt (interimObject.getString ("propertyValue"));
                    }
                    else if(interimObject.getString ("propertyName").equals ("subscription")){
                        if(!interimObject.getString ("propertyValue").equals ("0")){
                            workForSubs ();
                            usingSub = true;
                        }
                    }
                    else if(interimObject.getString ("propertyName").equals ("useAddOn")){
                        if(!interimObject.getString ("propertyValue").equals ("0")){
                            workForAddOn ();
                            usingAddOn = true;
                        }
                    }
                    else if(interimObject.getString ("propertyName").equals ("useGst")){
                        if(!interimObject.getString ("propertyValue").equals ("0")){
                            workForGST ();
                            usingGst =true;
                        }
                    }
                }
                isInitializedAccount = true;
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void workForGST(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"getgstratelist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        updateGst(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void workForAddOn(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"shopaddoninfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        udateAddOnInfo(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void workForSubs(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"shopsubsinfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        updateSubInfo(response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void updateSubInfo(JSONObject response){
            try{
                JSONArray dataSet = response.getJSONArray ("data");
                for(int i =0;i<dataSet.length ();i++){
                    JSONObject interimObject = dataSet.getJSONObject (i);
                    String addName = interimObject.getInt ("noOfDays") +"Days @ Rs "+interimObject.getInt ("suscriptionPrice");
                    subInformation.add (new addAddOnItemClass (addName,interimObject.getInt ("subscriptionId")));
                }
            }
            catch(JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void udateAddOnInfo(JSONObject response){
            try{
                JSONArray dataSet = response.getJSONArray ("data");
                for(int i =0;i<dataSet.length ();i++){
                    JSONObject interimObject = dataSet.getJSONObject (i);
                    String addName = interimObject.getString ("groupName") +" @ Rs "+interimObject.getInt ("groupCharge");
                    addOnInformation.add (new addAddOnItemClass (addName,interimObject.getInt ("groupId")));
                }
            }
            catch(JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void updateGst(JSONObject response){
            try{
                JSONArray dataSet = response.getJSONArray ("data");
                for(int i =0;i<dataSet.length ();i++){
                    JSONObject interimObject = dataSet.getJSONObject (i);
                    gstInfo.add (interimObject.getString ("gstName"));
                    gstId.add (interimObject.getInt ("gstId"));
                }
            }
            catch(JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    class initializeInsertion implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"showshopitemcat";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                JSONArray catList = response.getJSONArray ("data");
                catInformtion.add ("Kindly select a category");
                for(int i=0;i<catList.length ();i++){
                    JSONObject interimObject = catList.getJSONObject (i);
                    JSONArray catItemList = interimObject.getJSONArray ("catItemList");
                    for(int j=0;j<catItemList.length ();j++){
                        JSONObject finalInterim = catItemList.getJSONObject (j);
                        catInformtion.add (finalInterim.getString ("itemCatName"));
                        catId.add (finalInterim.getInt ("itemCatId"));
                    }
                }
                isCatInitialized = true;
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    class displayPrimartyList implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"itemlist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                totalImages = 0;
                useData = new ArrayList<> ();
                if(response.getInt ("totalCount")>0){
                    JSONArray dataSet = response.getJSONArray ("data");
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject mainItemGroup = dataSet.getJSONObject (i);
                        List<eachMenuItemClass> subMenuList = new ArrayList<> ();
                        JSONArray subItemList = mainItemGroup.getJSONArray ("subItems");
                        for(int j=0;j<subItemList.length ();j++){
                            JSONObject subItemGroup = subItemList.getJSONObject (j);
                            List<quanListClass> quantList = new ArrayList<> ();
                            JSONArray quantGroup = subItemGroup.getJSONArray ("quantInfo");
                            for(int k =0;k<quantGroup.length ();k++){
                                JSONObject quantView = quantGroup.getJSONObject (k);
                                quantList.add (new quanListClass (quantView.getString ("quantity"),quantView.getInt ("mrp"),quantView.getInt ("value"),quantView.getInt ("isActive"),quantView.getInt ("quantId")));
                            }
                            int isImage = 0;
                            if(!subItemGroup.getString ("imgUrl").equals ("")){
                                totalImages = totalImages+1;
                                isImage = 1;
                            }
                            subMenuList.add (new eachMenuItemClass (quantList,subItemGroup.getString ("Name"),subItemGroup.getInt("isActive"),subItemGroup.getInt ("subItemId"),mainItemGroup.getInt ("mainItemId"),isImage));
                        }
                        useData.add (new eachMenuMainClass (mainItemGroup.getString ("Name"),mainItemGroup.getInt ("mainItemId"),mainItemGroup.getInt ("isActive"),subMenuList));
                    }
                    final eachMenuMainAdapter adapter = new eachMenuMainAdapter (mCtx, useData,workingMainItem);
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            menuList.setAdapter (adapter);
                            workingOnSomething = true;
                        }
                    });
                }
                else{
                    workingOnSomething = true;
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            Toast.makeText (getApplicationContext (), "No items yet, kindly start adding", Toast.LENGTH_LONG).show ();
                        }
                    });
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void changeMainItemView(int mainItemId){
        FloatingActionButton  subItemAdd = findViewById (R.id.addonadd);
        FloatingActionButton  mainItemAdd = findViewById (R.id.addboy);
        if(mainItemId==workingMainItem){
            workingMainItem = 0;
            eachMenuMainAdapter adapter = new eachMenuMainAdapter (mCtx, useData,workingMainItem);
            menuList.setAdapter (adapter);
            subItemAdd.setVisibility (View.INVISIBLE);
            mainItemAdd.setVisibility (View.VISIBLE);
        }
        else{
            workingMainItem = mainItemId;
            eachMenuMainAdapter adapter = new eachMenuMainAdapter (mCtx, useData,workingMainItem);
            menuList.setAdapter (adapter);
            subItemAdd.setVisibility (View.VISIBLE);
            mainItemAdd.setVisibility (View.INVISIBLE);
        }
    }

    public void goBackToSettings(View view){
        goToOrders();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, shopSettings.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }

    public void createMainItem(View view){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
            final View mView = getLayoutInflater().inflate(R.layout.add_main_item, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            final Spinner mainCat = mView.findViewById (R.id.mainCat);
            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mCtx,android.R.layout.simple_spinner_item, catInformtion);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mainCat.setAdapter(dataAdapter);
            TextView createNew = mView.findViewById (R.id.createNew);
            createNew.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int isWorking = mainCat.getSelectedItemPosition ();
                    if(isWorking>0){
                        EditText catName = mView.findViewById (R.id.catName);
                        String catNameUse = catName.getText ().toString ();
                        if(catNameUse.equals ("")){
                            Toast.makeText (getApplicationContext (), "Kindly enter display name", Toast.LENGTH_SHORT).show ();
                        }
                        else{
                            isWorking = isWorking-1;
                            int catIdUse = catId.get (isWorking);
                            insertNewMainItem itemRunner = new insertNewMainItem(catNameUse,catIdUse, dialog);
                            workingOnSomething = false;
                            new Thread (itemRunner).start ();
                        }
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Kindly select item category", Toast.LENGTH_SHORT).show ();
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
        }
    }

    class insertNewMainItem implements Runnable{
        private String catName;
        private int catId;
        private AlertDialog dialog;

        public insertNewMainItem(String catName, int catId, AlertDialog dialog) {
            this.catName = catName;
            this.catId = catId;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("itemCatId",catId);
                requestObject.put ("mItemName",catName);
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"addmainitem";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "New category created", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Issue while creating category", Toast.LENGTH_SHORT).show ();
                            }
                        });
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void insertNewSubItem(View view){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            if(workingMainItem>0){
                workingGst = 1;
                addOnListItem = "";
                subListItem = "";
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                final View mView = getLayoutInflater().inflate(R.layout.add_sub_item, null);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                if(usingGst){
                    Spinner gstvalues = mView.findViewById (R.id.gstvalues);
                    ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(mCtx,android.R.layout.simple_spinner_item, gstInfo);
                    dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    gstvalues.setAdapter(dataAdapter);
                }
                else{
                    LinearLayout gstView = mView.findViewById (R.id.workingGST);
                    gstView.setVisibility (View.GONE);
                }
                if(usingAddOn){
                    RecyclerView displayingAddOn = mView.findViewById (R.id.displayingAddOn);
                    displayingAddOn.setHasFixedSize (false);
                    displayingAddOn.setNestedScrollingEnabled (false);
                    displayingAddOn.setLayoutManager (new LinearLayoutManager (mCtx));
                    addOnItemAddAdapter adapter = new addOnItemAddAdapter (mCtx,addOnInformation);
                    displayingAddOn.setAdapter (adapter);
                }
                else{
                    LinearLayout workingADDON = mView.findViewById (R.id.workingADDON);
                    workingADDON.setVisibility (View.GONE);
                }
                if(usingSub){
                    RecyclerView displayingSub = mView.findViewById (R.id.displayingSubInfo);
                    displayingSub.setHasFixedSize (false);
                    displayingSub.setNestedScrollingEnabled (false);
                    displayingSub.setLayoutManager (new LinearLayoutManager (mCtx));
                    subItemAddAdapter adapter = new subItemAddAdapter (mCtx,subInformation);
                    displayingSub.setAdapter (adapter);
                }
                else{
                    LinearLayout workingSub = mView.findViewById (R.id.displayingSub);
                    workingSub.setVisibility (View.GONE);
                }
                List<String> spclCat = new ArrayList<> ();
                spclCat.add ("None");
                spclCat.add ("Veg");
                spclCat.add ("Non-Veg");
                final Spinner subsvalue = mView.findViewById (R.id.subsvalue);
                ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<>(mCtx,android.R.layout.simple_spinner_item, spclCat);
                dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subsvalue.setAdapter(dataAdapter1);
                TextView addButton = mView.findViewById (R.id.addButton);
                TextView cancelButton = mView.findViewById (R.id.cancelButton);
                cancelButton.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss ();
                    }
                });

                addButton.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        EditText itemName = mView.findViewById (R.id.itemquntname);
                        String useItemName = itemName.getText ().toString ();
                        if(useItemName.equals ("")){
                            itemName.setError ("Kindly pass item name");
                            itemName.findFocus ();
                        }
                        else{
                            EditText itemDesc = mView.findViewById (R.id.itemdesc);
                            String itemDescString = itemDesc.getText ().toString ();
                            if(itemDescString.equals ("")){
                                itemDesc.setError ("Kindly pass item description");
                                itemDesc.findFocus ();
                            }
                            else{
                                int spclPositionInt = subsvalue.getSelectedItemPosition ();
                                String spclPosition = "";
                                if(spclPositionInt==0){
                                    spclPosition = "none";
                                }
                                else if(spclPositionInt==1){
                                    spclPosition = "veg";
                                }
                                else if(spclPositionInt==2){
                                    spclPosition = "nonveg";
                                }
                                else{
                                    spclPosition = "none";
                                }
                                int gstIdUse = 1;
                                if(usingGst){
                                    Spinner gstvalues = mView.findViewById (R.id.gstvalues);
                                    int gstSelected = gstvalues.getSelectedItemPosition ();
                                    gstIdUse = gstId.get (gstSelected);
                                }

                                workingOnSomething = false;
                                createSubItem runner = new createSubItem (spclPosition,useItemName,itemDescString,gstIdUse,dialog);
                                new Thread (runner).start ();

                            }
                        }
                    }
                });
                dialog.show ();
            }
            else{
                Toast.makeText (getApplicationContext (), "No main item selected", Toast.LENGTH_SHORT).show ();
            }
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
        }
    }

    class createSubItem implements Runnable{
        String spclCat, itemNameCurrent, itemDesc;
        int gstId;
        AlertDialog dialog;

        public createSubItem(String spclCat, String itemNameCurrent, String itemDesc, int gstId, AlertDialog dialog) {
            this.spclCat = spclCat;
            this.itemNameCurrent = itemNameCurrent;
            this.itemDesc = itemDesc;
            this.gstId = gstId;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            if(workingMainItem>0){
                try{
                    //String [] addOnInfo = {};
                    ArrayList<String> addOnInfo= new ArrayList<> ();
                    if(!addOnListItem.equals ("")){
                        Log.i("WOW",String.valueOf ("WOW"));
                        String [] addOnInfo1 = addOnListItem.split (",");
                        for(int i = 0 ;i<addOnInfo1.length;i++){
                            addOnInfo.add (addOnInfo1[i]);
                        }
                    }
                    ArrayList<String> subsInfo= new ArrayList<> ();
                    //String [] subsInfo = {};
                    if(!subListItem.equals ("")){
                        String [] addOnInfo1 = addOnListItem.split (",");
                        for(int i = 0 ;i<addOnInfo1.length;i++){
                            subsInfo.add (addOnInfo1[i]);
                        }
                    }

                    SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                    final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                    JSONObject requestObject = new JSONObject ();
                    requestObject.put ("userType",sessionInfo.getString ("userType"));
                    requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                    requestObject.put ("userId",sessionInfo.getInt ("userId"));
                    requestObject.put ("itemDesc",itemDesc);
                    if(!subListItem.equals ("")){
                        requestObject.put("subsInfo",subsInfo);
                    }
                    else{
                        //subsInfo = null;
                        requestObject.put("subsInfo",new JSONArray ());
                    }
                    if(!subListItem.equals ("")){
                        requestObject.put ("addOnInfo",addOnInfo);
                    }
                    else{
                        //addOnInfo = null;
                        requestObject.put("addOnInfo",new JSONArray ());
                    }


                    requestObject.put ("mItemId",workingMainItem);
                    requestObject.put ("spclCat",spclCat);
                    requestObject.put ("subItemName",itemNameCurrent);
                    requestObject.put ("gstId",gstId);
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    Log.i("WOW",String.valueOf (requestObject));
                    String finalUrl = newClass.baseUrl+"addsubitem";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            displayPrimartyList runner = new displayPrimartyList();
                            new Thread (runner).start ();
                            runOnUiThread (new Runnable () {
                                @Override
                                public void run() {
                                    dialog.dismiss ();
                                    Toast.makeText (getApplicationContext (), "New item added", Toast.LENGTH_SHORT).show ();
                                }
                            });
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            runOnUiThread (new Runnable () {
                                @Override
                                public void run() {
                                    dialog.dismiss ();
                                    Toast.makeText (getApplicationContext (), "Issue while creating item", Toast.LENGTH_SHORT).show ();
                                }
                            });
                            displayPrimartyList runner = new displayPrimartyList();
                            new Thread (runner).start ();
                        }
                    }){
                        @Override
                        public Map getHeaders() throws AuthFailureError {
                            Map<String,String> params = new HashMap<> ();
                            params.put("Content-Type", "application/json");
                            try{
                                params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                            }
                            catch (JSONException jsonExcepion2){
                                jsonExcepion2.printStackTrace ();
                            }
                            return params;
                        }
                    };
                    mRequestQueue.add(jsonObjectRequest);
                }
                catch (JSONException jsonException){
                    jsonException.printStackTrace ();
                }
            }
            else{
                workingOnSomething = true;
                dialog.dismiss ();
                Toast.makeText (getApplicationContext (), "No main item selected", Toast.LENGTH_SHORT).show ();
            }
        }
    }

    public void handleSubClick(int subId, CheckBox clickCheck){
        addingNewSubscription runner = new addingNewSubscription(subId,clickCheck);
        new Thread (runner).start ();
    }

    class addingNewSubscription implements Runnable{
        int subIdPassed;
        CheckBox subCheck;

        public addingNewSubscription(int subIdPassed, CheckBox subCheck) {
            this.subIdPassed = subIdPassed;
            this.subCheck = subCheck;
        }

        @Override
        public void run(){
             if(addingNewSub){
                 addingNewSub = false;
                 if(subListItem.equals ("")){
                     subListItem = String.valueOf (subIdPassed);
                     subCheck.setChecked (true);
                     addingNewSub = true;
                 }
                 else{
                     String[] subListInterim = subListItem.split (",");
                     String workingInterim= "";
                     boolean isAdded = false;
                     for(int i =0;i<subListInterim.length;i++){
                         if(String.valueOf (subIdPassed).equals (subListInterim[i])){
                             isAdded = true;
                         }
                         else{
                             if(workingInterim.equals ("")){
                                 workingInterim = subListInterim[i];
                             }
                             else{
                                 workingInterim = workingInterim+","+subListInterim[i];
                             }
                         }
                     }
                     if(isAdded){
                         subListItem = workingInterim;
                         subCheck.setChecked (false);
                         addingNewSub = true;
                     }
                     else{
                         subListItem = subListItem+","+subIdPassed;
                         subCheck.setChecked (true);
                         addingNewSub = true;
                     }
                 }
             }
             else{
                 Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
             }
        }
    }

    public void handleAddOnClick(int subId, CheckBox clickCheck){
        addingNewAddOn runner = new addingNewAddOn (subId,clickCheck);
        new Thread (runner).start ();
    }

    class addingNewAddOn implements Runnable{
        int subIdPassed;
        CheckBox subCheck;

        public addingNewAddOn(int subIdPassed, CheckBox subCheck) {
            this.subIdPassed = subIdPassed;
            this.subCheck = subCheck;
        }

        @Override
        public void run(){
            if(addingNewAddOn){
                addingNewAddOn = false;
                if(addOnListItem.equals ("")){
                    addOnListItem = String.valueOf (subIdPassed);
                    subCheck.setChecked (true);
                    addingNewAddOn = true;
                }
                else{
                    String[] subListInterim = addOnListItem.split (",");
                    String workingInterim= "";
                    boolean isAdded = false;
                    for(int i =0;i<subListInterim.length;i++){
                        if(String.valueOf (subIdPassed).equals (subListInterim[i])){
                            isAdded = true;
                        }
                        else{
                            if(workingInterim.equals ("")){
                                workingInterim = subListInterim[i];
                            }
                            else{
                                workingInterim = workingInterim+","+subListInterim[i];
                            }
                        }
                    }
                    if(isAdded){
                        addOnListItem = workingInterim;
                        subCheck.setChecked (false);
                        addingNewAddOn = true;
                    }
                    else{
                        addOnListItem = addOnListItem+","+subIdPassed;
                        subCheck.setChecked (true);
                        addingNewAddOn = true;
                    }
                }
            }
            else{
                Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
            }
        }
    }

    public void updateSubItemName(int isActive, String itemName, final int mItemId, final int subItemId){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
            final View mView = getLayoutInflater().inflate(R.layout.edit_item_name, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            final EditText itemNameDisplay = mView.findViewById (R.id.updateItemName);
            itemNameDisplay.setText (itemName);
            final Switch isActiveDisplay = mView.findViewById (R.id.switch1);
            if(isActive==1){
                isActiveDisplay.setChecked (true);
            }
            else{
                isActiveDisplay.setChecked (false);
            }
            TextView cancelButton = mView.findViewById (R.id.cancelButton);
            cancelButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    workingOnSomething = true;
                    dialog.dismiss ();
                }
            });
            TextView confirmButton = mView.findViewById (R.id.confirmButton);
            confirmButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    String updateItemName = itemNameDisplay.getText ().toString ();
                    if(updateItemName.equals ("")){
                        itemNameDisplay.setError ("Item Name can't be blank");
                        itemNameDisplay.findFocus ();
                    }
                    else{
                        int isActiveUpdate = 0;
                        if(isActiveDisplay.isChecked ()){
                            isActiveUpdate = 1;
                        }
                        workingOnSomething = false;
                        updatingSubItemName runner = new updatingSubItemName(updateItemName,subItemId,mItemId,isActiveUpdate,dialog);
                        new Thread (runner).start ();
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
        }
    }

    class updatingSubItemName implements Runnable{
        String itemNameUpdate;
        int subItemId, mItemId, isActiveUpdate;
        AlertDialog dialog;

        public updatingSubItemName(String itemNameUpdate, int subItemId, int mItemId, int isActiveUpdate, AlertDialog dialog) {
            this.itemNameUpdate = itemNameUpdate;
            this.subItemId = subItemId;
            this.mItemId = mItemId;
            this.isActiveUpdate = isActiveUpdate;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("isActiveFl",isActiveUpdate);
                requestObject.put ("mItemId",mItemId);
                requestObject.put("subItemId",subItemId);
                requestObject.put ("subItemName",itemNameUpdate);
                requestObject.put ("updateType","subItem");
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"updateitem";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Item updated", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Issue while updating item", Toast.LENGTH_SHORT).show ();
                            }
                        });
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void addNewQuant(final int subItemId,final int mItemId){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            selectedMaxQuant = 1;
            if(mItemId==workingMainItem){
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                final View mView = getLayoutInflater().inflate(R.layout.add_quant_item, null);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                TextView canceButton = mView.findViewById (R.id.cancelButton);
                canceButton.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        workingOnSomething = true;
                        dialog.dismiss ();
                    }
                });
                Spinner subQuant = mView.findViewById (R.id.gstvalues);
                List<String> spclCat = new ArrayList<> ();
                for(int i = 0; i<totalSubQuant;i++){
                    spclCat.add (String.valueOf (i+1));
                }
                final RecyclerView displayForm = mView.findViewById (R.id.displayForm);
                displayForm.setLayoutManager (new LinearLayoutManager (mCtx));
                displayForm.setHasFixedSize (false);
                displayForm.setNestedScrollingEnabled (false);
                ArrayAdapter<String> dataAdapter1 = new ArrayAdapter<>(mCtx,android.R.layout.simple_spinner_item, spclCat);
                dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                subQuant.setAdapter(dataAdapter1);
                subQuant.setOnItemSelectedListener (new AdapterView.OnItemSelectedListener () {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        quantAddAdapter adapter1 = new quantAddAdapter (mCtx, position+1);
                        displayForm.setAdapter (adapter1);
                        selectedMaxQuant = position+1;
                        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                    }
                    @Override
                    public void onNothingSelected(AdapterView<?> parentView) {
                        // your code here
                        final quantAddAdapter adapter = new quantAddAdapter (mCtx, 1);
                        displayForm.setAdapter (adapter);
                        selectedMaxQuant = 1;
                        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE| WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);
                    }
                });
                TextView createButton = mView.findViewById (R.id.createButton);
                createButton.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        if(workingOnSomething){
                            try{
                                JSONArray quantInfo = new JSONArray ();
                                int isOk = 1;
                                for(int i=0;i<selectedMaxQuant;i++){
                                    String quantNameTag = "name"+i;
                                    String quantMrpTag = "mrp"+i;
                                    String quantValueTag = "value"+i;
                                    EditText quantName = mView.findViewWithTag (quantNameTag);
                                    EditText quantValue = mView.findViewWithTag (quantValueTag);
                                    EditText quantMrp = mView.findViewWithTag (quantMrpTag);
                                    String quantNameUse = quantName.getText ().toString ();
                                    String quantMrpUse = quantMrp.getText ().toString ();
                                    String quantValueUse = quantValue.getText ().toString ();
                                    if(quantNameUse.equals ("")){
                                        quantName.setError ("Kindly pass name for the quantity");
                                        quantName.findFocus ();
                                        isOk = 0;
                                        break;
                                    }
                                    else{
                                        if(quantMrpUse.equals ("")){
                                            quantMrp.setError ("Kindly pass mrp");
                                            quantMrp.findFocus ();
                                            isOk = 0;
                                            break;
                                        }
                                        else{
                                            if(quantValueUse.equals ("")){
                                                quantValue.setError ("Kindly pass value");
                                                quantValue.findFocus ();
                                                isOk = 0;
                                                break;
                                            }
                                            else{
                                                int mrpUse = Integer.parseInt (quantMrpUse);
                                                int valueUse = Integer.parseInt (quantValueUse);
                                                if(valueUse<=mrpUse){
                                                    JSONObject interimObject = new JSONObject ();
                                                    interimObject.put ("quantity",quantNameUse);
                                                    interimObject.put ("mrp",mrpUse);
                                                    interimObject.put ("value",valueUse);
                                                    quantInfo.put (interimObject);
                                                }
                                                else{
                                                    quantValue.setError ("Value can't be more than MRP");
                                                    quantValue.findFocus ();
                                                    isOk = 0;
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                }
                                if(isOk==1){
                                    workingOnSomething = false;
                                    creatingNewQuant runner = new creatingNewQuant(quantInfo,mItemId,subItemId,selectedMaxQuant,dialog);
                                    new Thread (runner).start ();
                                }
                            }
                            catch (JSONException jsonException){
                                jsonException.printStackTrace ();
                            }
                        }
                        else{
                            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
                        }
                    }
                });
                dialog.show ();
            }
            else{
                Toast.makeText (getApplicationContext (), "Uh-oh something is not right", Toast.LENGTH_SHORT).show ();
            }
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
        }
    }

    class creatingNewQuant implements Runnable{
        JSONArray quantInfo;
        int mItemId, subItemId,totalCountQuant;
        AlertDialog dialog;

        public creatingNewQuant(JSONArray quantInfo, int mItemId, int subItemId, int totalCountQuant, AlertDialog dialog) {
            this.quantInfo = quantInfo;
            this.mItemId = mItemId;
            this.subItemId = subItemId;
            this.totalCountQuant = totalCountQuant;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("mItemId",mItemId);
                requestObject.put ("subItemId",subItemId);
                requestObject.put ("quantInfo",quantInfo);
                requestObject.put ("totalQuantInfo",totalCountQuant);
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"insertquantityinfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Quantity added", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Issue while updating item", Toast.LENGTH_SHORT).show ();
                            }
                        });
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void displayQuant(List<quanListClass> quantInfo, int subItemId, int mItemId, String itemName){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
            final View mView = getLayoutInflater().inflate(R.layout.quant_display_item, null);
            mBuilder.setView(mView);
            final AlertDialog dialog = mBuilder.create();
            TextView itemNameUse = mView.findViewById (R.id.itemName);
            itemNameUse.setText (itemName);
            RecyclerView displayQuantList = mView.findViewById (R.id.displayQuantList);
            displayQuantList.setLayoutManager (new LinearLayoutManager (this));
            displayQuantList.setHasFixedSize (false);
            displayQuantList.setNestedScrollingEnabled (false);
            quantDisplayAdapter adapter = new quantDisplayAdapter (quantInfo,this,mItemId,subItemId,displayQuantList);
            displayQuantList.setAdapter (adapter);
            TextView cancelButton = mView.findViewById (R.id.cancelButton);
            cancelButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    workingOnSomething = true;
                }
            });
            dialog.show ();
        }
        else {
            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
        }
    }

    public void updateQuantInfo(final quanListClass container, final int mItemId, final int subItemId, final RecyclerView openSet, final List<quanListClass> useInfo, final int position){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
                final View mView = getLayoutInflater().inflate(R.layout.edit_quant, null);
                mBuilder.setView(mView);
                final AlertDialog dialog = mBuilder.create();
                final EditText quantName = mView.findViewById (R.id.itemquntname);
                final EditText quantMrp = mView.findViewById (R.id.itemmrp);
                final EditText quantValue = mView.findViewById (R.id.itemcost);
                quantMrp.setText (String .valueOf (container.getMrp ()));
                quantValue.setText (String.valueOf (container.getValueUse ()));
                quantName.setText (container.getQuantName ());
                TextView canceButton = mView.findViewById (R.id.cancelButton);
                canceButton.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss ();
                        workingOnSomething = true;
                    }
                });
                TextView updateButton = mView.findViewById (R.id.updateButton);
                updateButton.setOnClickListener (new View.OnClickListener () {
                    @Override
                    public void onClick(View v) {
                        String updatedName = quantName.getText ().toString ();
                        if(updatedName.equals ("")){
                            quantName.setError ("Name can't be left blank");
                            quantName.findFocus ();
                        }
                        else{
                            String interimMrp = quantMrp.getText ().toString ();
                            if(interimMrp.equals ("")){
                                quantMrp.setError ("MRP cannot be blank");
                                quantMrp.findFocus ();
                            }
                            else{
                                String interimCost = quantValue.getText ().toString ();
                                if(interimCost.equals ("")){
                                    quantValue.setError ("Bill amount cannot be left blank");
                                    quantValue.findFocus ();
                                }
                                else{
                                    int mrpCurrent = Integer.parseInt (interimMrp);
                                    int costCurrent = Integer.parseInt (interimCost);
                                    if(costCurrent>mrpCurrent){
                                        quantValue.setError ("Cost cannot be more than MRP");
                                        quantValue.findFocus ();
                                    }
                                    else{
                                        container.setMrp (mrpCurrent);
                                        container.setQuantName (updatedName);
                                        container.setValueUse (costCurrent);
                                        useInfo.set (position,container);
                                        updateQuantInformation runner = new updateQuantInformation(useInfo,mItemId,subItemId,container,openSet,dialog);
                                        workingOnSomething = false;
                                        new Thread (runner).start ();
                                    }
                                }
                            }
                        }
                    }
                });
                dialog.show ();
            }
            else{
                Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
            }
        }
    }

    class updateQuantInformation implements Runnable{
        List<quanListClass> useInfo;
        int mItemId, subItemId;
        quanListClass containerUse;
        RecyclerView updateInfo;
        AlertDialog dialog;

        public updateQuantInformation(List<quanListClass> useInfo, int mItemId, int subItemId, quanListClass containerUse, RecyclerView updateInfo, AlertDialog dialog) {
            this.useInfo = useInfo;
            this.mItemId = mItemId;
            this.subItemId = subItemId;
            this.containerUse = containerUse;
            this.updateInfo = updateInfo;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("mItemId",mItemId);
                requestObject.put ("subItemId",subItemId);
                requestObject.put ("mrp",containerUse.getMrp ());
                requestObject.put ("quantId",containerUse.getQuantId ());
                requestObject.put ("quantity",containerUse.getQuantName ());
                requestObject.put ("updateType","quantity");
                requestObject.put ("value",containerUse.getValueUse ());
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"updateitem";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                quantDisplayAdapter adapter = new quantDisplayAdapter (useInfo,mCtx,mItemId,subItemId,updateInfo);
                                updateInfo.setAdapter (adapter);
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Quantity updated", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Issue while updating information", Toast.LENGTH_SHORT).show ();
                            }
                        });
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void updateImage(eachMenuItemClass container){
        if(workingOnSomething&&isCatInitialized&&isInitializedAccount){
            if(container.getIsImage ()==1){
                finalUploadImage(container);
            }
            else{
                if(totalImages<=allowedImages){
                    finalUploadImage(container);
                }
            }
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be patient, another process already working", Toast.LENGTH_LONG).show ();
        }
    }

    public void finalUploadImage(final eachMenuItemClass container){
        itemUpload=null;
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mCtx);
        final View mView = getLayoutInflater().inflate(R.layout.add_item_image, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        TextView browseImage = mView.findViewById (R.id.browseImage);

        browseImage.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
            }
        });
        TextView uploadButton = mView.findViewById (R.id.uploadButton);
        uploadButton.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if(itemUpload==null){
                    Toast.makeText (getApplicationContext (), "Kindly select and Image for upload", Toast.LENGTH_LONG).show ();
                }
                else{
                    if(shouldAskPermissions ()){
                        askPermissions();
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Image being uploaded, please remain patient", Toast.LENGTH_LONG).show ();
                        workingOnSomething = false;
                        uploadingImageItem runner = new uploadingImageItem(container,dialog);
                        new Thread (runner).start ();
                    }
                }
            }
        });

        TextView cancelButton = mView.findViewById (R.id.cancelButton);
        cancelButton.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                workingOnSomething = true;
            }
        });
        dialog.show ();
    }

    class uploadingImageItem implements Runnable{
        eachMenuItemClass container;
        AlertDialog dialog;

        public uploadingImageItem(eachMenuItemClass container,AlertDialog dialog) {
            this.container = container;
            this.dialog = dialog;
        }

        @Override
        public void run() {
           try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                String url = newClass.baseUrl+"insertitemimage";
                uploadImage imageUpload = new uploadImage (mCtx,itemUpload,sessionInfo.getInt ("typeId"),sessionInfo.getInt ("userId"),container.getSubItemId (),sessionInfo.getString ("userType"),sessionInfo.getString ("basicAuthenticate"),url);
                try{
                    if(imageUpload.run ()){
                        dialog.dismiss ();
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Image uploaded successfully", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                    else {
                        dialog.dismiss ();
                        displayPrimartyList runner = new displayPrimartyList();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Uh-oh something is not right", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }
                catch (IOException ioException){
                    ioException.printStackTrace ();
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE && resultCode == Activity.RESULT_OK) {
            if (data == null) {
                itemUpload = null;
                return;
            }
            else{
                itemUpload = data.getData();
            }
        }
    }

    @TargetApi(23)
    private void askPermissions() {
        String[] permissions = {
                "android.permission.READ_EXTERNAL_STORAGE",
                "android.permission.WRITE_EXTERNAL_STORAGE"
        };
        int requestCode = 200;
        requestPermissions(permissions, requestCode);
    }

    private boolean shouldAskPermissions() {
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.GET_ACCOUNTS);
        if (result == PackageManager.PERMISSION_GRANTED) {
            return true;
        } else {
            return false;
        }
    }
}
