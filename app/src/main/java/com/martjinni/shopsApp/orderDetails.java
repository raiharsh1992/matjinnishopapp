package com.martjinni.shopsApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class orderDetails extends AppCompatActivity {
    String returnPage;
    RecyclerView displayItem;
    TextView orderIdView, customerName, totalAmount, orderStatus, paymentMethod, orderDate, orderTime, addLine1, addLine2, addLine3, totalItemCount, itemCost, gstCost, billCost;
    int orderId;
    LinearLayout displayNew;
    Context mCtx;
    volleyClass newClass = new volleyClass ();
    public volatile boolean isOrderWorking = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_order_details);
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            displayNew = findViewById (R.id.displayNew);
            mCtx = this;
            displayNew.bringToFront ();
            returnPage = (getIntent ().getStringExtra ("fromTab"));
            if(returnPage.equals ("OTHERS")){
                displayNew.setVisibility (View.GONE);
            }
            workWithInformation runnable = new workWithInformation ();
            new Thread (runnable).start ();
            orderId = Integer.valueOf (getIntent ().getStringExtra ("orderId"));
            orderIdView = findViewById (R.id.orderId);
            customerName = findViewById (R.id.customerName);
            totalAmount = findViewById (R.id.totalAmount);
            orderStatus = findViewById (R.id.orderStatus);
            paymentMethod = findViewById (R.id.paymentMethod);
            orderDate = findViewById (R.id.orderDate);
            orderTime = findViewById (R.id.orderTime);
            addLine1 = findViewById (R.id.addLine1);
            addLine2 = findViewById (R.id.addLine2);
            addLine3 = findViewById (R.id.addLine3);
            totalItemCount = findViewById (R.id.itemCount);
            itemCost = findViewById (R.id.itemCost);
            gstCost = findViewById (R.id.gstCost);
            billCost = findViewById (R.id.totalOrderCost);
            displayItem = findViewById (R.id.displayShopItems);
            displayItem.setHasFixedSize (false);
            displayItem.setNestedScrollingEnabled (false);
            displayItem.setLayoutManager (new LinearLayoutManager (this));
        }
    }

    class workWithInformation implements Runnable{

        @Override
        public void run(){
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("orderId",String.valueOf (orderId));
                String finalUrl = newClass.baseUrl+"orderdetails";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders(){
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(final JSONObject response){
            try{
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        try{
                            customerName.setText (response.getString ("custName"));
                            addLine1.setText (response.getString ("addLine1"));
                            addLine2.setText (response.getString ("addLine2"));
                            paymentMethod.setText (response.getString ("paymentMethod"));
                            float itemCostUse = Float.parseFloat (String.valueOf (response.getDouble ("amount")-response.getDouble ("gstAmount")));
                            totalAmount.setText (String .valueOf (response.getDouble ("amount")));
                            itemCost.setText (String .valueOf (itemCostUse));
                            gstCost.setText (String.valueOf (response.getDouble ("gstAmount")));
                            billCost.setText (String .valueOf (response.getDouble ("amount")));
                            String addLine3Use = response.getString ("city")+" , "+response.getString ("state");
                            addLine3.setText (addLine3Use);
                            String orderIdUse = "Order Id: "+orderId;
                            orderIdView.setText (orderIdUse);
                            orderStatus.setText (response.getString ("orderStatus"));
                            if(response.getString ("orderStatus").equals ("NEW")){
                                displayNew.setVisibility (View.VISIBLE);
                            }
                            else{
                                displayNew.setVisibility (View.GONE);
                            }
                            String [] dateTime = response.getString ("creationDate").split ("T");
                            orderDate.setText (dateTime[0]);
                            orderTime.setText (dateTime[1]);
                            totalItemCount.setText (String.valueOf (response.getInt ("itemCount")));
                        }
                        catch (JSONException jsonException){
                            jsonException.printStackTrace ();
                        }
                    }
                });
                JSONArray items = response.getJSONArray ("items");
                final List<viewClassOrderDetailsItem> information = new ArrayList<> ();
                for(int i=0;i<items.length ();i++){
                    JSONObject viewItem = items.getJSONObject (i);
                    int subsDays = viewItem.optInt ("subsDay",0);
                    int displaySubs = 0, displayAddOn = 0;
                    String addOnName = "", subsStartDate = "", subsDelTime = "", daysLeft = "";
                    if(subsDays>0){
                        daysLeft = String.valueOf (viewItem.optInt ("subDaysLeft",0));
                        String [] useSubsDate = viewItem.getString ("subStartDate").split ("T");
                        subsStartDate = useSubsDate[0];
                        subsDelTime = useSubsDate[1];
                        displaySubs = 1;
                    }
                    if(!viewItem.getString ("addOnName").equals ("")){
                        displayAddOn = 1;
                        addOnName = viewItem.getString ("addOnName").substring (1);
                    }
                    String displayAmount = "Rs. "+viewItem.getDouble ("amount");
                    information.add (new viewClassOrderDetailsItem (viewItem.getString ("itemName"),String.valueOf (viewItem.getInt ("quantity")),displayAmount,addOnName,subsStartDate,subsDelTime,daysLeft,displayAddOn,displaySubs));
                }
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        viewAdapterOrderDetailsItem adapter = new viewAdapterOrderDetailsItem (mCtx,information);
                        displayItem.setAdapter (adapter);
                        isOrderWorking = true;
                    }
                });
            }
            catch (JSONException jsonException1){
                jsonException1.printStackTrace ();
            }
        }
    }

    class updateOrderStatus implements Runnable{
        @Override
        public void run(){
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("orderId",orderId);
                requestObject.put ("orderStatus","ACCEPTED");
                String finalUrl = newClass.baseUrl+"updateorderstatus";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation ();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders(){
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
        private void displayInformation(){
            runOnUiThread (new Runnable () {
                @Override
                public void run() {
                    displayNew.setVisibility (View.GONE);
                    workWithInformation runnable = new workWithInformation ();
                    new Thread (runnable).start ();
                }
            });
        }
    }

    class cancelOrderThread implements Runnable{
        @Override
        public void run(){
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("orderId",orderId);
                String finalUrl = newClass.baseUrl+"cancelorder";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation ();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders(){
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
        private void displayInformation(){
            runOnUiThread (new Runnable () {
                @Override
                public void run() {
                    displayNew.setVisibility (View.GONE);
                    workWithInformation runnable = new workWithInformation ();
                    new Thread (runnable).start ();
                }
            });
        }
    }

    public void acceptOrder(View view){
        if(isOrderWorking){
            isOrderWorking = false;
            updateOrderStatus runnable = new updateOrderStatus ();
            new Thread (runnable).start ();
        }
    }

    public void cancelOrder(View view){
        if(isOrderWorking){
            isOrderWorking = false;
            cancelOrderThread runnable = new cancelOrderThread ();
            new Thread (runnable).start ();
        }
    }

    public void backPressed(View view){
        goToOrders ();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, orders.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }
}
