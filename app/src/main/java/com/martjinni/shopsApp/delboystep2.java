package com.martjinni.shopsApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.martjinni.Appshops.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class delboystep2 extends Fragment {

    public RequestQueue mrequest;
    TextView fragmentAction;
    EditText fullname;
    EditText email;
    EditText phonenumber;
    String fn = "";
    String em = "";
    String pn = "";
    volleyClass newClass = new volleyClass();
    public String baseUrl = newClass.baseUrl;

    public delboystep2() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_delboystep2, container, false);
        fragmentAction = view.findViewById(R.id.fragmentaction);
        fullname = view.findViewById(R.id.fullname);
        email = view.findViewById(R.id.email);
        phonenumber = view.findViewById(R.id.number);

        fragmentAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fn = fullname.getText().toString();
                em = email.getText().toString();
                pn = phonenumber.getText().toString();

                if (fn.equals("") || em.equals("") || pn.equals("")) {
                    ((adddelboy) getActivity()).alertshow("Please fill all the fields");

                } else if (pn.length() < 10) {
                    ((adddelboy) getActivity()).alertshow("Please fill all the fields");
                }
                else if(fn.length()<3){
                    ((adddelboy) getActivity()).alertshow("Name must be greater than 3 characters");
                }
                else if(email.length()<10){
                    ((adddelboy) getActivity()).alertshow("Email must be greater than 10 characters");
                }
                else{
                    ((adddelboy)getActivity()).validatenumber(pn, fn, em);
                }
            }

        });
        return view;
    }

}
