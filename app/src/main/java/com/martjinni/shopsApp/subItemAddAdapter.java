package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.martjinni.Appshops.R;

import java.util.List;

public class subItemAddAdapter extends RecyclerView.Adapter<subItemAddAdapter.subItemViewHolder>{
    Context mCtx;
    List<addAddOnItemClass> useInfo;

    public subItemAddAdapter(Context mCtx, List<addAddOnItemClass> useInfo) {
        this.mCtx = mCtx;
        this.useInfo = useInfo;
    }

    @NonNull
    @Override
    public subItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.select_add_on_item,null);
        return new subItemViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull subItemViewHolder holder, int position) {
        addAddOnItemClass container = useInfo.get (position);
        holder.addOn.setText (container.getDisplayName ());
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class subItemViewHolder extends RecyclerView.ViewHolder{
        CheckBox addOn;
        public subItemViewHolder(View itemView){
            super(itemView);
            addOn = itemView.findViewById (R.id.displayAddon);
            addOn.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    addAddOnItemClass container = useInfo.get (position);
                    ((itemsPage)mCtx).handleSubClick(container.getAddOnId (),addOn);
                }
            });
        }
    }

}
