package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class eachMenuMainAdapter extends RecyclerView.Adapter<eachMenuMainAdapter.eachMenuMainViewHolder>{
    Context mCtx;
    List<eachMenuMainClass> userData;
    int displayList;

    public eachMenuMainAdapter(Context mCtx, List<eachMenuMainClass> userData, int displayList) {
        this.mCtx = mCtx;
        this.userData = userData;
        this.displayList = displayList;
    }

    @NonNull
    @Override
    public eachMenuMainViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.item_list_menu,null);
        return new eachMenuMainViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull eachMenuMainViewHolder holder, int position) {
        eachMenuMainClass container = userData.get (position);
        holder.mainItemName.setText (container.getMainItemName ());
        eachMenuItemAdapter adapter = new eachMenuItemAdapter (mCtx,container.getUserInfo ());
        holder.eachItemList.setNestedScrollingEnabled (false);
        holder.eachItemList.setHasFixedSize (false);
        holder.eachItemList.setLayoutManager (new LinearLayoutManager (mCtx));
        holder.eachItemList.setAdapter (adapter);
        if(displayList==container.getMainItemId ()){
            holder.displaySubList.setVisibility (View.VISIBLE);
        }
    }

    @Override
    public int getItemCount() {
        return userData.size ();
    }


    class eachMenuMainViewHolder extends RecyclerView.ViewHolder{
        TextView mainItemName;
        LinearLayout displaySubList;
        RecyclerView eachItemList;
        public eachMenuMainViewHolder(View itemView){
            super(itemView);
            mainItemName = itemView.findViewById (R.id.mainItemName);
            displaySubList = itemView.findViewById (R.id.displayingMenu);
            eachItemList = itemView.findViewById (R.id.displayItemMenu);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    eachMenuMainClass container = userData.get (position);
                    ((itemsPage)mCtx).changeMainItemView(container.getMainItemId ());
                }
            });
        }
    }
}
