package com.martjinni.shopsApp;

public class addAddOnItemClass {
    private String displayName;
    private int addOnId;

    public addAddOnItemClass(String displayName, int addOnId) {
        this.displayName = displayName;
        this.addOnId = addOnId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public int getAddOnId() {
        return addOnId;
    }
}
