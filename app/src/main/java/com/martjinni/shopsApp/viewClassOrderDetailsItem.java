package com.martjinni.shopsApp;

public class viewClassOrderDetailsItem {
    private String itemName, quantity, totalCost, addOnName, susbStartDate, subsDelTime, subsDaysLeft;
    private int displayAddOn, displaySubs;

    public viewClassOrderDetailsItem(String itemName, String quantity, String totalCost, String addOnName, String susbStartDate, String subsDelTime, String subsDaysLeft, int displayAddOn, int displaySubs) {
        this.itemName = itemName;
        this.quantity = quantity;
        this.totalCost = totalCost;
        this.addOnName = addOnName;
        this.susbStartDate = susbStartDate;
        this.subsDelTime = subsDelTime;
        this.subsDaysLeft = subsDaysLeft;
        this.displayAddOn = displayAddOn;
        this.displaySubs = displaySubs;
    }

    public String getItemName() {
        return itemName;
    }

    public String getQuantity() {
        return quantity;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public String getAddOnName() {
        return addOnName;
    }

    public String getSusbStartDate() {
        return susbStartDate;
    }

    public String getSubsDelTime() {
        return subsDelTime;
    }

    public String getSubsDaysLeft() {
        return subsDaysLeft;
    }

    public int getDisplayAddOn() {
        return displayAddOn;
    }

    public int getDisplaySubs() {
        return displaySubs;
    }
}
