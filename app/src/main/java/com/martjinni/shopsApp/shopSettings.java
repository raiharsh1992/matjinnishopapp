package com.martjinni.shopsApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class shopSettings extends AppCompatActivity {
    volleyClass newClass = new volleyClass ();
    Switch shopStatus;
    LinearLayout displayCustomIf;
    TextView manageSubs, manageAddOn, managePromo;
    public int displayCustom = 0, displayAddon = 0, displaySubs = 0, displayPromo = 0;
    public volatile boolean workingStatus = false;
    public volatile int shopOnline = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_shop_settings);
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        shopStatus = findViewById (R.id.simpleSwitch);
        shopStatus.setChecked (false);
        displayCustomIf = findViewById (R.id.customView);
        manageSubs = findViewById (R.id.manageSubs);
        manageAddOn = findViewById (R.id.manageAddOn);
        managePromo = findViewById (R.id.managePromo);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            inializeStoreStatus runnable = new inializeStoreStatus ();
            new Thread (runnable).start ();
            initializingMenu runnable2 = new initializingMenu ();
            new Thread (runnable2).start ();
        }
        shopStatus.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                if(workingStatus){
                    workingStatus = false;
                    updateStoreStatus runnable3 = new updateStoreStatus ();
                    new Thread (runnable3).start ();
                }
                else{
                    shopStatus.toggle ();
                    Toast.makeText(getApplicationContext(),"Already working on previous request", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    class initializingMenu implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"getshopproperty";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        displayCustomIf.setVisibility (View.GONE);
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
        private void displayInformation(JSONObject response){
            try{
                JSONArray dataSet = response.getJSONArray ("data");
                for(int i=0;i<dataSet.length ();i++){
                    JSONObject interimObject = dataSet.getJSONObject (i);
                    if(interimObject.getString ("propertyName").equals ("subscription")){
                        int propertyValue = Integer.valueOf (interimObject.getString ("propertyValue"));
                        if(propertyValue>0){
                            displayAddon = 1;
                            displayCustom = 1;
                        }
                    }
                    else if(interimObject.getString ("propertyName").equals ("useAddOn")){
                        int propertyValue = Integer.valueOf (interimObject.getString ("propertyValue"));
                        if(propertyValue>0){
                            displaySubs = 1;
                            displayCustom = 1;
                        }
                    }
                    else if(interimObject.getString ("propertyName").equals ("maxPromoCount")){
                        int propertyValue = Integer.valueOf (interimObject.getString ("propertyValue"));
                        if(propertyValue>0){
                            displayPromo = 1;
                            displayCustom = 1;
                        }
                    }
                }
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        if(displayCustom==0){
                            displayCustomIf.setVisibility (View.GONE);
                        }
                        else if(displayAddon==0){
                            manageAddOn.setVisibility (View.GONE);
                        }
                        else if(displaySubs==0){
                            manageSubs.setVisibility (View.GONE);
                        }
                        else if(displayPromo==0){
                            managePromo.setVisibility (View.GONE);
                        }
                    }
                });
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    class inializeStoreStatus implements Runnable{

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put("userType",sessionInfo.getString ("userType"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"getshopstatus";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders(){
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
        private void displayInformation(JSONObject response){
            try{
                final int storeStatus = response.getInt ("storeStatus");
                runOnUiThread (new Runnable () {
                    @Override
                    public void run() {
                        if(storeStatus==1){
                            shopStatus.setChecked (true);
                        }
                        else{
                            shopStatus.setChecked (false);
                        }
                    }
                });
                shopOnline = storeStatus;
                workingStatus = true;
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    class updateStoreStatus implements Runnable{

        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                int newShopStatus = 0;
                if(shopOnline==0){
                    newShopStatus = 1;
                }
                JSONObject requestObject = new JSONObject ();
                requestObject.put("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put("userType",sessionInfo.getString ("userType"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("storeStatus",newShopStatus);
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"updateshopstatus";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        inializeStoreStatus runnable = new inializeStoreStatus ();
                        new Thread (runnable).start ();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        inializeStoreStatus runnable = new inializeStoreStatus ();
                        new Thread (runnable).start ();
                    }
                }){
                    @Override
                    public Map getHeaders(){
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void backPressed(View view){
        goToOrders ();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, orders.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }

    public void goToDeliveryBoy(View view){
        Intent intent = new Intent(this, deliveryBoy.class);
        startActivity(intent);
        finish();
    }

    public void goToPromo(View view){
        Intent intent = new Intent(this, promoCode.class);
        startActivity(intent);
        finish();
    }

    public void goToAddOn(View view){
        Intent intent = new Intent(this, addOnPage.class);
        startActivity(intent);
        finish();
    }

    public void goToItems(View view){
        Intent intent = new Intent(this, itemsPage.class);
        startActivity(intent);
        finish();
    }

    public void goToSubscription(View view){
        Intent intent = new Intent(this, subscriptionView.class);
        startActivity(intent);
        finish();
    }

    public void logoutClick(View view){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        TextView errorMessage = mView.findViewById (R.id.errormessage);
        String displayMessage = "Do you wish to logout?";
        errorMessage.setText (displayMessage);
        TextView continueView = mView.findViewById (R.id.modalclose);
        continueView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                logoutFinal ();
            }
        });
        TextView cancelView = mView.findViewById (R.id.modalContinue);
        cancelView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
        dialog.show ();
    }

    public void logoutFinal(){
        disableNotif ();
    }

    public void logoutFromSystem(){
        final Context mCtx = this;
        final SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        try{
            final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
            JSONObject requestObject = new JSONObject ();
            requestObject.put("userType",sessionInfo.getString ("userType"));
            requestObject.put ("userId",sessionInfo.getInt ("userId"));
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            RequestQueue mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            String finalUrl = newClass.baseUrl+"signout";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    sharedPreferences.edit ().clear ().apply ();
                    Intent intent = new Intent(mCtx, orderDetails.class);
                    startActivity(intent);
                    finish ();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    sharedPreferences.edit ().remove ("sessionInfo").apply ();
                    Intent intent = new Intent(mCtx, orderDetails.class);
                    startActivity(intent);
                    finish ();
                }
            }){
                @Override
                public Map getHeaders(){
                    Map<String,String> params = new HashMap<> ();
                    params.put("Content-Type", "application/json");
                    try{
                        params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                    }
                    catch (JSONException jsonExcepion2){
                        jsonExcepion2.printStackTrace ();
                    }
                    return params;
                }
            };
            mRequestQueue.add(jsonObjectRequest);
        }
        catch (JSONException jsonException){
            sharedPreferences.edit ().remove ("sessionInfo").apply ();
            Intent intent = new Intent(mCtx, orderDetails.class);
            startActivity(intent);
            finish ();
            jsonException.printStackTrace ();
        }
    }

    public void disableNotif(){
        try{
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            if(!sharedPreferences.getString ("sessionInfo","").equals ("")){
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                String token = sharedPreferences.getString ("userMessageToken","");
                if(!token.equals ("")){
                    requestObject.put ("userType",sessionInfo.getString ("userType"));
                    requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                    requestObject.put ("userId",sessionInfo.getInt ("userId"));
                    requestObject.put ("token",token);
                    Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                    Network network = new BasicNetwork (new HurlStack ());
                    RequestQueue mRequestQueue = new RequestQueue (cache, network);
                    mRequestQueue.start();
                    String finalUrl = newClass.baseUrl+"remusertoken";
                    JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            logoutFromSystem ();
                        }
                    }, new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            logoutFromSystem ();
                        }
                    }){
                        @Override
                        public Map getHeaders() {
                            Map<String,String> params = new HashMap<> ();
                            params.put("Content-Type", "application/json");
                            try{
                                params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                            }
                            catch (JSONException jsonExcepion2){
                                jsonExcepion2.printStackTrace ();
                            }
                            return params;
                        }
                    };
                    mRequestQueue.add(jsonObjectRequest);
                }
                else{
                    logoutFromSystem ();
                }
            }
            else{
                logoutFromSystem ();
            }
        }
        catch (JSONException jsonException){
            logoutFromSystem ();
            jsonException.printStackTrace ();
        }
    }
    public void changepass(View view){
        Intent intent = new Intent(shopSettings.this, changepassword.class);
        startActivity(intent);
        finish();
    }
}
