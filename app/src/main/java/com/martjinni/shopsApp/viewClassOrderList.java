package com.martjinni.shopsApp;

public class viewClassOrderList {
    private String customerName, totalAmount, orderStatus, payMethod;
    private int orderId, displayAssign;

    public viewClassOrderList(String customerName, String totalAmount, String orderStatus, String payMethod, int orderId, int displayAssign) {
        this.customerName = customerName;
        this.totalAmount = totalAmount;
        this.orderStatus = orderStatus;
        this.payMethod = payMethod;
        this.orderId = orderId;
        this.displayAssign = displayAssign;
    }

    public String getCustomerName() {
        return customerName;
    }

    public String getTotalAmount() {
        return totalAmount;
    }

    public String getOrderStatus() {
        return orderStatus;
    }

    public String getPayMethod() {
        return payMethod;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getDisplayAssign() {
        return displayAssign;
    }
}
