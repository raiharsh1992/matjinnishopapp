package com.martjinni.shopsApp;

public class displayShopTImeClass {
    private String shopTime, timeName, timeDesc;
    private int timeId, isActive;

    public displayShopTImeClass(String shopTime, String timeName, String timeDesc, int timeId, int isActive) {
        this.shopTime = shopTime;
        this.timeName = timeName;
        this.timeDesc = timeDesc;
        this.timeId = timeId;
        this.isActive = isActive;
    }

    public String getShopTime() {
        return shopTime;
    }

    public String getTimeName() {
        return timeName;
    }

    public String getTimeDesc() {
        return timeDesc;
    }

    public int getTimeId() {
        return timeId;
    }

    public int getIsActive() {
        return isActive;
    }
}
