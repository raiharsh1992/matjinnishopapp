package com.martjinni.shopsApp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class login extends AppCompatActivity {
    private static final String TAG = "THISPAGE";
    EditText shopname;
    EditText shoppass;
    TextView login;
    TextView newuser;

    public RequestQueue mRequestQueue;
    volleyClass newClass = new volleyClass();
    public String baseUrl = newClass.baseUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
         shopname = findViewById(R.id.username);
         shoppass = findViewById(R.id.shoppass);
         login = findViewById(R.id.loginbtn);
         newuser = findViewById(R.id.newuser);
         login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateLogin();
            }
        });
         newuser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                newUser();
            }
        });
    }

    public void forgotPassword(View view){
        Intent intent = new Intent(this, changepassword.class);
        startActivity(intent);
        finish();
    }

    private void validateLogin(){
        String username = shopname.getText().toString();
        String password = shoppass.getText().toString();

        if (username.equals("") || password.equals("")){

            Toast.makeText(this, "Please Fill the required Fields", Toast.LENGTH_LONG).show();
        }
       else{
            try {
                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                mRequestQueue = new RequestQueue(cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject();
                jsonVal.put("userName", username);
                jsonVal.put("password", password);
                jsonVal.put("mode", "shop");
                String userUrl = baseUrl + "login";
                Log.i(TAG,userUrl);

                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        shoppass.clearFocus();
                        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPreferences.edit();
                        editor.putString ("sessionInfo",response.toString ()).apply ();
                        loginSuccess();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        shoppass.clearFocus();
                        Log.d("error", error.toString());
                        Toast.makeText(getApplicationContext(), "Username/Password ", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }
        }

    }
    private void loginSuccess(){
        applyNewToken runner = new applyNewToken ();
        new Thread (runner).start ();
        Intent intent = new Intent(this, orders.class);
        startActivity(intent);
        finish();
    }
    private void newUser(){

    }

    class applyNewToken implements Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                if(!sharedPreferences.getString ("sessionInfo","").equals ("")){
                    final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                    JSONObject requestObject = new JSONObject ();
                    String token = sharedPreferences.getString ("userMessageToken","");
                    if(!token.equals ("")){
                        requestObject.put ("userType",sessionInfo.getString ("userType"));
                        requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                        requestObject.put ("userId",sessionInfo.getInt ("userId"));
                        requestObject.put ("token",token);
                        Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                        Network network = new BasicNetwork (new HurlStack ());
                        RequestQueue mRequestQueue = new RequestQueue (cache, network);
                        mRequestQueue.start();
                        String finalUrl = newClass.baseUrl+"setusertoken";
                        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {

                            }
                        }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                error.printStackTrace ();
                            }
                        }){
                            @Override
                            public Map getHeaders() throws AuthFailureError {
                                Map<String,String> params = new HashMap<> ();
                                params.put("Content-Type", "application/json");
                                try{
                                    params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                                }
                                catch (JSONException jsonExcepion2){
                                    jsonExcepion2.printStackTrace ();
                                }
                                return params;
                            }
                        };
                        mRequestQueue.add(jsonObjectRequest);
                    }
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void exit(){
        android.os.Process.killProcess(android.os.Process.myPid());
        System.exit(1);
        super.onBackPressed ();
    }

    public void exit1(){
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        TextView errorMessage = mView.findViewById (R.id.errormessage);
        String displayMessage = "Do you wish to exit?";
        errorMessage.setText (displayMessage);
        TextView continueView = mView.findViewById (R.id.modalclose);
        continueView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
                exit ();
            }
        });
        TextView cancelView = mView.findViewById (R.id.modalContinue);
        cancelView.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog.dismiss ();
            }
        });
        dialog.show ();
    }

    @Override
    public void onBackPressed()
    {
        exit1 ();
    }
}
