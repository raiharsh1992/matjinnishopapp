package com.martjinni.shopsApp;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class subscriptionView extends AppCompatActivity {
    RecyclerView subList;
    volleyClass newClass = new volleyClass ();
    List<subListClass> useInfo = new ArrayList<> ();
    Context mCtx;
    String selectedTIme = "";
    public volatile boolean workingOnSomethin = false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_subscription_view);
        subList = findViewById (R.id.displayAllSubs);
        subList.setNestedScrollingEnabled (false);
        subList.setHasFixedSize (false);
        subList.setLayoutManager (new LinearLayoutManager (this));
        mCtx = this;
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            Intent intent = new Intent(this, login.class);
            startActivity(intent);
            finish();
        }
        else{
            generatingNewSubList runnable = new generatingNewSubList ();
            new Thread (runnable).start ();
        }
    }

    class generatingNewSubList implements Runnable{
        @Override
        public void run(){
            useInfo = new ArrayList<> ();
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"shopsubsinfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        workingOnSomethin = true;
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                JSONArray dataSet = response.getJSONArray ("data");
                if(dataSet.length ()>0){
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject interim = dataSet.getJSONObject (i);
                        useInfo.add (new subListClass (interim.getInt ("noOfDays"),interim.getInt ("isActive"),interim.getInt ("suscriptionPrice"),interim.getInt ("noOfDays")));
                    }
                    final subListAdapter adapter = new subListAdapter (mCtx, useInfo);
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            subList.setAdapter (adapter);
                            workingOnSomethin = true;
                        }
                    });
                }
                else{
                    workingOnSomethin = true;
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            Toast.makeText (getApplicationContext (), "No subscription yet, kindly start adding", Toast.LENGTH_LONG).show ();
                        }
                    });
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void goBackToSettings(View view){
        goToOrders();
    }

    public void goToOrders(){
        Intent intent = new Intent(this, shopSettings.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToOrders ();
        super.onBackPressed();  // optional depending on your needs
    }

    public void onCreateNewPackage(View view){
        if(workingOnSomethin){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
            final View mView = getLayoutInflater ().inflate (R.layout.subs_create_package, null);
            mBuilder.setView (mView);
            final AlertDialog dialog = mBuilder.create ();
            dialog.setCanceledOnTouchOutside (false);
            TextView continueUse = mView.findViewById (R.id.continueAdd);
            TextView cancelUse = mView.findViewById (R.id.cancelButton);
            cancelUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    workingOnSomethin = true;
                    dialog.dismiss ();
                }
            });
            continueUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    EditText subDays = mView.findViewById (R.id.itemquntname);
                    String subDayCount = subDays.getText ().toString ();
                    if(!subDayCount.equals ("")){
                        EditText subPrice = mView.findViewById (R.id.itemmrp);
                        String subPriceValue = subPrice.getText ().toString ();
                        if(subPriceValue.equals ("")){
                            subPrice.setError ("Kindly enter valid price");
                            subPrice.findFocus ();
                        }
                        else{
                            insertingNewPackage runner = new insertingNewPackage (Integer.parseInt (subDayCount), Integer.parseInt (subPriceValue), dialog);
                            new Thread (runner).start ();
                            workingOnSomethin = false;
                        }
                    }
                    else{
                        subDays.setError ("Kindly enter no. of days");
                        subDays.findFocus ();
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be paitent, system is already working on something", Toast.LENGTH_LONG).show ();
        }
    }

    class insertingNewPackage implements Runnable{
        int dayCount, price;
        AlertDialog dialog;

        public insertingNewPackage(int dayCount, int price, AlertDialog dialog) {
            this.dayCount = dayCount;
            this.price = price;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("noOfDays",dayCount);
                requestObject.put ("subValue",price);
                String finalUrl = newClass.baseUrl+"insertsubsinfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generatingNewSubList runner = new generatingNewSubList ();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Subscription created", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Subscription already exist, please try a different day count", Toast.LENGTH_LONG).show ();
                                dialog.dismiss ();
                                workingOnSomethin = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void updateSubInfo(int isActive, final int dayCount, final int subPrice){
        if(workingOnSomethin){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
            final View mView = getLayoutInflater ().inflate (R.layout.edit_sub_info, null);
            mBuilder.setView (mView);
            final AlertDialog dialog = mBuilder.create ();
            dialog.setCanceledOnTouchOutside (false);
            TextView continueUse = mView.findViewById (R.id.continueAdd);
            TextView cancelUse = mView.findViewById (R.id.cancelButton);
            TextView subDayCount = mView.findViewById (R.id.itemquntname);
            subDayCount.setText (String .valueOf (dayCount));
            final EditText subPriceUse = mView.findViewById (R.id.itemmrp);
            subPriceUse.setText (String.valueOf (subPrice));
            final Switch isActiveFl = mView.findViewById (R.id.isActive);
            if(isActive==1){
                isActiveFl.setChecked (true);
            }
            else{
                isActiveFl.setChecked (false);
            }
            cancelUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    workingOnSomethin = true;
                }
            });
            continueUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    String updatedPrice = subPriceUse.getText ().toString ();
                    if(updatedPrice.equals ("")){
                        subPriceUse.setError ("Kindly pass a value for subscription package");
                        subPriceUse.findFocus ();
                    }
                    else{
                        int isActiveNew = 0;
                        if(isActiveFl.isChecked ()){
                            isActiveNew = 1;
                        }
                        updateExistingPackage runner = new updateExistingPackage(isActiveNew, dayCount, Integer.parseInt (updatedPrice),dialog);
                        new Thread (runner).start ();
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be paitent, system is already working on something", Toast.LENGTH_LONG).show ();
        }
    }

    class updateExistingPackage implements Runnable{
        int isActive, noOfDays, subPrice;
        AlertDialog dialog;

        public updateExistingPackage(int isActive, int noOfDays, int subPrice, AlertDialog dialog) {
            this.isActive = isActive;
            this.noOfDays = noOfDays;
            this.subPrice = subPrice;
            this.dialog = dialog;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("noOfDays",noOfDays);
                requestObject.put ("subValue",subPrice);
                requestObject.put ("isActiveFl",isActive);
                String finalUrl = newClass.baseUrl+"updatesubsinfo";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generatingNewSubList runner = new generatingNewSubList ();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Subscription updated", Toast.LENGTH_SHORT).show ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Subscription already exist, please try a different day count", Toast.LENGTH_LONG).show ();
                                dialog.dismiss ();
                                workingOnSomethin = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void displaySubTime(View view){
        if(workingOnSomethin){
            AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
            final View mView = getLayoutInflater ().inflate (R.layout.sub_time_list, null);
            mBuilder.setView (mView);
            final AlertDialog dialog = mBuilder.create ();
            dialog.setCanceledOnTouchOutside (false);
            TextView continueUse = mView.findViewById (R.id.continueAdd);
            TextView cancelUse = mView.findViewById (R.id.cancelButton);
            final RecyclerView timeListData = mView.findViewById (R.id.displayList);
            timeListData.setHasFixedSize (false);
            timeListData.setLayoutManager (new LinearLayoutManager (mCtx));
            displayingTime runner = new displayingTime (timeListData);
            new Thread (runner).start ();
            workingOnSomethin = true;
            cancelUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    dialog.dismiss ();
                    workingOnSomethin = true;
                }
            });
            continueUse.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    if(workingOnSomethin){
                        Log.i("wow","kya baat");
                        newTime (timeListData);
                    }
                    else{
                        Toast.makeText (getApplicationContext (), "Please be paitent, system is already working on something", Toast.LENGTH_LONG).show ();
                    }
                }
            });
            dialog.show ();
        }
        else{
            Toast.makeText (getApplicationContext (), "Please be paitent, system is already working on something", Toast.LENGTH_LONG).show ();
        }
    }

    class displayingTime implements Runnable{
        RecyclerView displayTime;

        public displayingTime(RecyclerView displayTime) {
            this.displayTime = displayTime;
        }
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("shopId",sessionInfo.getInt ("typeId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"viewshoptime";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        workingOnSomethin = true;
                    }
                });
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }

        private void displayInformation(JSONObject response){
            try{
                if(response.getInt ("totalCount")>0){
                    List<displayShopTImeClass> useInfo = new ArrayList<> ();
                    JSONArray dataSet = response.getJSONArray ("Data");
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject interimObject = dataSet.getJSONObject (i);
                        useInfo.add (new displayShopTImeClass (interimObject.getString ("time"),interimObject.getString ("name"),interimObject.getString ("desc"),interimObject.getInt ("timeId"),interimObject.getInt ("isActive")));
                    }
                    final List<displayShopTImeClass> userInfo = useInfo;
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            displayShopTImeAdapter adapter = new displayShopTImeAdapter (mCtx, userInfo, displayTime);
                            displayTime.setAdapter (adapter);
                            workingOnSomethin = true;
                        }
                    });
                }
                else{
                    workingOnSomethin = true;
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            Toast.makeText (getApplicationContext (), "No time set yet, start by adding the magic", Toast.LENGTH_LONG).show ();
                        }
                    });
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void newTime(final RecyclerView letsSee){
        selectedTIme = "";
        AlertDialog.Builder mBuilder = new AlertDialog.Builder (mCtx);
        final View mView = getLayoutInflater ().inflate (R.layout.create_new_time_sub, null);
        mBuilder.setView (mView);
        final AlertDialog dialog1 = mBuilder.create ();
        TextView continueUse = mView.findViewById (R.id.continueAdd);
        TextView cancelUse = mView.findViewById (R.id.cancelButton);
        final TextView selectTime = mView.findViewById (R.id.selectTime);
        final EditText timeDesc = mView.findViewById (R.id.addoncost);
        final EditText timeName = mView.findViewById (R.id.addonname);
        selectTime.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(mCtx, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        selectTime.setText( selectedHour + ":" + selectedMinute);
                        selectedTIme = selectedHour + ":" + selectedMinute;
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
        });
        cancelUse.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                dialog1.dismiss ();
                workingOnSomethin = true;
            }
        });
        continueUse.setOnClickListener (new View.OnClickListener () {
            @Override
            public void onClick(View v) {
                String timeNameUse = timeName.getText ().toString ();
                if(timeNameUse.equals ("")){
                    timeName.setError ("Kindly pass a time name");
                    timeName.findFocus ();
                }
                else{
                    String timeDescUse = timeDesc.getText ().toString ();
                    if(timeDescUse.equals ("")){
                        timeDesc.setError ("Kindly pass time description");
                        timeDesc.findFocus ();
                    }
                    else{
                        if(selectedTIme.equals ("")){
                            selectTime.setError ("Kindly select a time value");
                            selectTime.findFocus ();
                        }
                        else{
                            insertingNewShopTime runnable3 = new insertingNewShopTime(letsSee, dialog1, timeNameUse,  timeDescUse,selectedTIme);
                            new Thread (runnable3).start ();
                            workingOnSomethin = false;
                        }
                    }
                }
            }
        });
        dialog1.show ();
    }

    class insertingNewShopTime implements Runnable{
        RecyclerView existingList;
        AlertDialog dialog;
        String tiemNameUse, itemDesc, timeValue;

        public insertingNewShopTime(RecyclerView existingList, AlertDialog dialog, String tiemNameUse, String itemDesc, String timeValue) {
            this.existingList = existingList;
            this.dialog = dialog;
            this.tiemNameUse = tiemNameUse;
            this.itemDesc = itemDesc;
            this.timeValue = timeValue;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("name",tiemNameUse);
                requestObject.put ("time",timeValue);
                requestObject.put ("desc",itemDesc);
                String finalUrl = newClass.baseUrl+"insertshoptime";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generatingNewSubList runner = new generatingNewSubList ();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                dialog.dismiss ();
                                Toast.makeText (getApplicationContext (), "Time added successfully", Toast.LENGTH_SHORT).show ();
                                displayingTime runner = new displayingTime(existingList);
                                new Thread (runner).start ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Subscription already exist, please try a different day count", Toast.LENGTH_LONG).show ();
                                dialog.dismiss ();
                                workingOnSomethin = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }

    public void updateTimeStatus(int timeId, String timeValue, String timeName, String timeDesc, int updateStatus, Switch workingState, RecyclerView displayTime){
        if(workingOnSomethin){
            updateShopTimeStatus runner = new updateShopTimeStatus(timeId, updateStatus, timeValue, timeName, timeDesc, displayTime);
            new Thread (runner).start ();
            workingOnSomethin = false;
        }
        else{
            workingState.toggle ();
        }
    }

    class updateShopTimeStatus implements Runnable{
        int timeId, isActive;
        String timeValue, timeName, timeDesc;
        RecyclerView mView;

        public updateShopTimeStatus(int timeId, int isActive, String timeValue, String timeName, String timeDesc, RecyclerView mView) {
            this.timeId = timeId;
            this.isActive = isActive;
            this.timeValue = timeValue;
            this.timeName = timeName;
            this.timeDesc = timeDesc;
            this.mView = mView;
        }

        @Override
        public void run(){
            try{
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                requestObject.put ("name",timeName);
                requestObject.put ("time",timeValue);
                requestObject.put ("desc",timeDesc);
                requestObject.put ("isActive",isActive);
                requestObject.put ("timeId",timeId);
                String finalUrl = newClass.baseUrl+"updateshoptime";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        generatingNewSubList runner = new generatingNewSubList ();
                        new Thread (runner).start ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Time updated successfully", Toast.LENGTH_SHORT).show ();
                                displayingTime runner = new displayingTime(mView);
                                new Thread (runner).start ();
                            }
                        });
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                        runOnUiThread (new Runnable () {
                            @Override
                            public void run() {
                                Toast.makeText (getApplicationContext (), "Issue while updating time", Toast.LENGTH_LONG).show ();
                                workingOnSomethin = true;
                            }
                        });
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);

            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }
}
