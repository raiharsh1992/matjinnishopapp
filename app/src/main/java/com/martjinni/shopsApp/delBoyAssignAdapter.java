package com.martjinni.shopsApp;

import android.app.AlertDialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class delBoyAssignAdapter  extends RecyclerView.Adapter<delBoyAssignAdapter.delBoyAssignViewHolder>{
    private Context mCtx;
    private List<delBoyListClass> useList;
    private AlertDialog mView;
    private int orderId;
    public delBoyAssignAdapter(Context mCtx, List<delBoyListClass> useList, AlertDialog mView, int OrderId) {
        this.mCtx = mCtx;
        this.useList = useList;
        this.mView = mView;
        this.orderId = OrderId;
    }

    @NonNull
    @Override
    public delBoyAssignViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.layout_delivery_boy_list,null);
        return new delBoyAssignViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull delBoyAssignViewHolder holder, int position) {
        delBoyListClass container = useList.get (position);
        holder.delBoyName.setText (container.getDelBoyName ());
        holder.delPhone.setText (container.getDelPhone ());
    }

    @Override
    public int getItemCount() {
        return useList.size ();
    }

    class delBoyAssignViewHolder extends RecyclerView.ViewHolder{
        TextView delBoyName, delPhone;
        public delBoyAssignViewHolder(View itemView){
            super(itemView);
            delBoyName = itemView.findViewById (R.id.delboyname);
            delPhone = itemView.findViewById (R.id.editphone);
            itemView.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    ((orders)mCtx).acceptOrder (mView, position, orderId);
                }
            });
        }
    }
}
