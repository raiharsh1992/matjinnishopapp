package com.martjinni.shopsApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.martjinni.Appshops.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class fragment_getpassword extends Fragment {

    EditText pass1;
    EditText pass2;
    TextView action;

    public fragment_getpassword() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_restore_getpassword, container, false);
        pass1 = view.findViewById(R.id.getpassword);
        pass2 = view.findViewById(R.id.getpassword2);
        action = view.findViewById(R.id.chngpassaction);
        action.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String pas1 = pass1.getText().toString();
                String pas2 = pass2.getText().toString();
                if(!pas1.equals(pas2))
                {
                    ((changepassword)getActivity()).alertshow("Entered Password Do not match! Please enter again ");

                }
                else if(pas1.length()<8)
                {
                    ((changepassword)getActivity()).alertshow("Password Must be larger than 8 characters");
                }
                else if(pas1.length()>24){
                    ((changepassword)getActivity()).alertshow("Password Must be smaller than 24 characters");
                }
                else {
                    ((changepassword)getActivity()).changepass(pas1);

                }
            }
        });
        return view;
    }

}
