package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class quantDisplayAdapter extends RecyclerView.Adapter<quantDisplayAdapter.quantDisplayViewHolder>{

    List<quanListClass> useInfo;
    Context mCtx;
    int mItemId, subItemId;
    RecyclerView informationHolder;

    public quantDisplayAdapter(List<quanListClass> useInfo, Context mCtx, int mItemId, int subItemId, RecyclerView informationHolder) {
        this.useInfo = useInfo;
        this.mCtx = mCtx;
        this.mItemId = mItemId;
        this.subItemId = subItemId;
        this.informationHolder = informationHolder;
    }

    @NonNull
    @Override
    public quantDisplayViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.quant_view_item_list,null);
        return new quantDisplayViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull quantDisplayViewHolder holder, int position) {
        quanListClass container = useInfo.get (position);
        holder.itemquntname.setText (container.getQuantName ());
        holder.itemmrp.setText (String .valueOf (container.getMrp ()));
        holder.itemcost.setText (String .valueOf (container.getValueUse ()));
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class quantDisplayViewHolder extends RecyclerView.ViewHolder{
        TextView itemquntname, itemmrp, itemcost,editButton;
        public quantDisplayViewHolder(View itemView){
            super(itemView);
            itemquntname = itemView.findViewById (R.id.itemquntname);
            itemmrp = itemView.findViewById (R.id.itemmrp);
            itemcost = itemView.findViewById (R.id.itemcost);
            editButton = itemView.findViewById (R.id.editButton);
            editButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    quanListClass container = useInfo.get (position);
                    ((itemsPage)mCtx).updateQuantInfo(container,mItemId,subItemId,informationHolder,useInfo,position);
                }
            });
        }
    }
}
