package com.martjinni.shopsApp;


import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.util.Log;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class uploadImage {
    Context mCtx;
    Uri uri;
    int typeId, userId, itemId;
    String userType, basicAuth, finalUrl;
    private static final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/jpeg");

    public uploadImage(Context mCtx, Uri uri, int typeId, int userId, int itemId, String userType, String basicAuth, String finalUrl) {
        this.mCtx = mCtx;
        this.uri = uri;
        this.typeId = typeId;
        this.userId = userId;
        this.itemId = itemId;
        this.userType = userType;
        this.basicAuth = basicAuth;
        this.finalUrl = finalUrl;
    }

    public boolean run() throws IOException{
        ContentResolver resolver = mCtx.getContentResolver();
        String path = getFilePathFromContentUri(uri,resolver); // "file:///mnt/sdcard/FileName.mp3"
        OkHttpClient client = new OkHttpClient();
        RequestBody bodyUse = new MultipartBody.Builder ().setType (MultipartBody.FORM)
                .addFormDataPart("userId",String.valueOf (userId))
                .addFormDataPart ("typeId",String .valueOf (typeId))
                .addFormDataPart ("userType",String.valueOf (userType))
                .addFormDataPart ("subItemId",String.valueOf (itemId))
                .addFormDataPart ("proPic",getFileName (uri),RequestBody.create(MEDIA_TYPE_PNG, new File (path)))
                .build ();
        Request request1 = new Request.Builder ()
                .url(finalUrl)
                .post(bodyUse)
                .addHeader("content-type", "multipart/form-data; boundary=----WebKitFormBoundary7MA4YWxkTrZu0gW")
                .addHeader("basicauthenticate", basicAuth)
                .addHeader("cache-control", "no-cache")
                .build();
        Response response = client.newCall(request1).execute();


        Log.i ("WOW",getFileName (uri));
        if(response.isSuccessful ()){
            return true;
        }
        else{
            return false;
        }
    }

    private String getFileName(Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = mCtx.getContentResolver().query(uri, null, null, null, null);
            try {
                if (cursor != null && cursor.moveToFirst()) {
                    result = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                }
            } finally {
                cursor.close();
            }
        }
        if (result == null) {
            result = uri.getPath();
            int cut = result.lastIndexOf('/');
            if (cut != -1) {
                result = result.substring(cut + 1);
            }
        }
        return result;
    }

    private String getFilePathFromContentUri(Uri selectedVideoUri,
                                             ContentResolver contentResolver) {
        String filePath;
        String[] filePathColumn = {MediaStore.MediaColumns.DATA};

        Cursor cursor = contentResolver.query(selectedVideoUri, filePathColumn, null, null, null);
        cursor.moveToFirst();

        int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
        filePath = cursor.getString(columnIndex);
        cursor.close();
        return filePath;
    }
}
