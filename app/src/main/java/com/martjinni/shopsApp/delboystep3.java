package com.martjinni.shopsApp;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.martjinni.Appshops.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class delboystep3 extends Fragment {
    TextView fragmentAction;
    LinearLayout resendOtp;
    EditText otp;

    public delboystep3() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_delboystep3, container, false);
        otp = view.findViewById(R.id.otp);
        resendOtp = view.findViewById(R.id.resendotp);
        fragmentAction = view.findViewById(R.id.fragmentaction);
        resendOtp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((adddelboy)getActivity()).generateotp();
            }
        });

        fragmentAction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String otpsrtr =otp.getText().toString();
                if(otpsrtr.length() != 6){
                    ((adddelboy)getActivity()).alertshow("OTP must be of 6 digits");
                }
                else{
                    ((adddelboy)getActivity()).validateotp(otpsrtr);
                }
            }
        });
        return view;
    }

}
