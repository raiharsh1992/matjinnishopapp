package com.martjinni.shopsApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class deliveryBoy extends AppCompatActivity {
    private static final String TAG = "WOW";
    RecyclerView displayDelBoyList;
    Context mCxt;
    volleyClass newClass = new volleyClass ();
    List<delBoyListClass> useAdd = new ArrayList<> ();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate (savedInstanceState);
        setContentView (R.layout.activity_delivery_boy);
        displayDelBoyList = findViewById (R.id.displayDelBoyList);
        displayDelBoyList.setHasFixedSize (false);
        displayDelBoyList.setNestedScrollingEnabled (false);
        displayDelBoyList.setLayoutManager (new LinearLayoutManager (this));
        mCxt = this;
        SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
        if(sharedPreferences.getString ("sessionInfo","").equals ("")){
            goToSettings ();
        }
        else{
            createDelBoyList runnable = new createDelBoyList ();
            new Thread (runnable).start ();
        }
        FloatingActionButton addboy = findViewById(R.id.addboy);
        addboy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkIfCreationPoss();
            }
        });
    }

    public void checkIfCreationPoss(){
        try{
            Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork (new HurlStack ());
            RequestQueue mRequestQueue = new RequestQueue (cache, network);
            mRequestQueue.start();
            SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
            final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
            JSONObject requestObject = new JSONObject ();
            requestObject.put ("userType",sessionInfo.getString ("userType"));
            requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
            requestObject.put ("userId",sessionInfo.getInt ("userId"));
            requestObject.put("propertyName","maxDelboyCount");
            String finalUrl = newClass.baseUrl+"validateshopprop";
            Log.i(TAG,String.valueOf (requestObject));
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Intent intent = new Intent(deliveryBoy.this, adddelboy.class);
                    startActivity(intent);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText (getApplicationContext (), "Max number of delivery boys already present", Toast.LENGTH_SHORT).show ();
                }
            }){
                @Override
                public Map getHeaders() throws AuthFailureError {
                    Map<String,String> params = new HashMap<> ();
                    params.put("Content-Type", "application/json");
                    try{
                        params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                    }
                    catch (JSONException jsonExcepion2){
                        jsonExcepion2.printStackTrace ();
                    }
                    return params;
                }
            };
            mRequestQueue.add(jsonObjectRequest);
        }
        catch (JSONException jsonException){
            jsonException.printStackTrace ();
        }
    }

    class createDelBoyList implements  Runnable{
        @Override
        public void run(){
            try{
                SharedPreferences sharedPreferences = (getApplicationContext()).getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                JSONObject requestObject = new JSONObject ();
                requestObject.put ("userType",sessionInfo.getString ("userType"));
                requestObject.put ("typeId",sessionInfo.getInt ("typeId"));
                requestObject.put ("userId",sessionInfo.getInt ("userId"));
                Cache cache = new DiskBasedCache (getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork (new HurlStack ());
                RequestQueue mRequestQueue = new RequestQueue (cache, network);
                mRequestQueue.start();
                String finalUrl = newClass.baseUrl+"getdmlist";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest (Request.Method.POST, finalUrl, requestObject,  new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        displayInformation (response);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace ();
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<> ();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
        public void displayInformation(JSONObject response){
            try{
                if(response.getInt ("totalCount")>0){
                    JSONArray dataSet = response.getJSONArray ("dmList");
                    for(int i=0;i<dataSet.length ();i++){
                        JSONObject useInfo = dataSet.getJSONObject (i);
                        String phoneNumber = "+91"+useInfo.getInt ("dmPhone");
                        useAdd.add (new delBoyListClass (useInfo.getString ("dmName"),phoneNumber,useInfo.getInt ("dmId")));
                    }
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            delBoyListAdapter adapter = new delBoyListAdapter (mCxt,useAdd);
                            displayDelBoyList.setAdapter (adapter);
                        }
                    });
                }
                else{
                    runOnUiThread (new Runnable () {
                        @Override
                        public void run() {
                            Toast.makeText (getApplicationContext (), "No delivery boy added, kindly start with adding new", Toast.LENGTH_LONG).show ();
                        }
                    });
                }
            }
            catch (JSONException jsonException){
                jsonException.printStackTrace ();
            }
        }
    }


    public void backPressed(View view){
        goToSettings ();
    }

    public void goToSettings(){
        Intent intent = new Intent(this, shopSettings.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed()
    {
        goToSettings ();
        super.onBackPressed();  // optional depending on your needs
    }
}
