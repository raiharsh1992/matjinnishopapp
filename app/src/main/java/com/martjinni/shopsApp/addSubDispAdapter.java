package com.martjinni.shopsApp;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class addSubDispAdapter  extends  RecyclerView.Adapter<addSubDispAdapter.addSubDispViewHolder>{
    Context mCtx;
    private List<addSubDispClass> useData;

    public addSubDispAdapter(Context mCtx, List<addSubDispClass> useData) {
        this.mCtx = mCtx;
        this.useData = useData;
    }

    @NonNull
    @Override
    public addSubDispViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.addon_sub_list,null);
        return new addSubDispViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull addSubDispViewHolder holder, int position) {
        addSubDispClass container = useData.get (position);
        if(container.getSubStatus ()==0){
            holder.subStatus.setText ("IN-ACTIVE");
            holder.subStatus.setTextColor (Color.parseColor ("#bad70303"));
        }
        else{
            holder.subStatus.setText ("ACTIVE");
            holder.subStatus.setTextColor (Color.parseColor ("#ba7ed321"));
        }
        holder.subName.setText (container.getSubName ());
    }

    @Override
    public int getItemCount() {
        return useData.size ();
    }

    class addSubDispViewHolder extends RecyclerView.ViewHolder{
        TextView subName, subStatus;
        ImageView editButton;
        public addSubDispViewHolder(View itemView){
            super(itemView);
            subName = itemView.findViewById (R.id.subName);
            subStatus = itemView.findViewById (R.id.subStatus);
            editButton = itemView.findViewById (R.id.subEdit);
            editButton.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    addSubDispClass container = useData.get (position);
                    ((addOnPage)mCtx).addOnItem(container.getMasterId (),container.getSubStatus (),container.getSubName ());
                }
            });
        }
    }
}
