package com.martjinni.shopsApp;

import android.content.Context;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.martjinni.Appshops.R;

import java.util.List;

public class subListAdapter extends RecyclerView.Adapter<subListAdapter.subListViewHolder>{

    Context mCtx;
    List<subListClass> useInfo;

    public subListAdapter(Context mCtx, List<subListClass> useInfo) {
        this.mCtx = mCtx;
        this.useInfo = useInfo;
    }

    @NonNull
    @Override
    public subListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.subs_list_card,null);
        return new subListViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull subListViewHolder holder, int position) {
        subListClass container = useInfo.get (position);
        holder.subPrice.setText (String.valueOf (container.getSubPrice ()));
        holder.noOfDays.setText (String.valueOf (container.getNoOfDays ()));
        if(container.getIsActive ()==0){
            holder.subStatus.setText ("IN-ACTIVE");
            holder.subStatus.setTextColor (Color.parseColor ("#bad70303"));
        }
        else{
            holder.subStatus.setText ("ACTIVE");
            holder.subStatus.setTextColor (Color.parseColor ("#ba7ed321"));
        }
    }

    @Override
    public int getItemCount() {
        return useInfo.size ();
    }

    class subListViewHolder extends RecyclerView.ViewHolder{
        TextView noOfDays, subStatus, subPrice, subEdit;
        public subListViewHolder(View itemView){
            super(itemView);
            noOfDays = itemView.findViewById (R.id.noOfDays);
            subStatus = itemView.findViewById (R.id.subStaus);
            subPrice = itemView.findViewById (R.id.subsPrice);
            subEdit = itemView.findViewById (R.id.subEdit);
            subEdit.setOnClickListener (new View.OnClickListener () {
                @Override
                public void onClick(View v) {
                    int position = getAdapterPosition ();
                    subListClass container = useInfo.get (position);
                    ((subscriptionView)mCtx).updateSubInfo(container.getIsActive (),container.getNoOfDays (),container.getSubPrice ());
                }
            });
        }
    }
}
