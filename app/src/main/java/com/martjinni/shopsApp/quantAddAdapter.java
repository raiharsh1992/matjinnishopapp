package com.martjinni.shopsApp;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.martjinni.Appshops.R;

public class quantAddAdapter extends RecyclerView.Adapter<quantAddAdapter.quantAddViewHolder>{

    Context mCtx;
    int displayCount;

    public quantAddAdapter(Context mCtx, int displayCount) {
        this.mCtx = mCtx;
        this.displayCount = displayCount;
    }

    @NonNull
    @Override
    public quantAddViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from (mCtx);
        View view = inflater.inflate (R.layout.add_quant_form_layout,null);
        return new quantAddViewHolder (view);
    }

    @Override
    public void onBindViewHolder(@NonNull quantAddViewHolder holder, int position) {
        int currentCount = position;
        String quantNameTag = "name"+currentCount;
        String quantMrpTag = "mrp"+currentCount;
        String quantValueTag = "value"+currentCount;
        holder.quantValue.setTag (quantValueTag);
        holder.quantMrp.setTag (quantMrpTag);
        holder.quantName.setTag (quantNameTag);
    }

    @Override
    public int getItemCount() {
        return displayCount;
    }

    class quantAddViewHolder extends RecyclerView.ViewHolder{
        EditText quantName, quantMrp, quantValue;

        public quantAddViewHolder(View itemView){
            super(itemView);
            quantName = itemView.findViewById (R.id.itemquntname);
            quantMrp = itemView.findViewById (R.id.itemmrp);
            quantValue = itemView.findViewById (R.id.itemcost);
        }
    }
}
