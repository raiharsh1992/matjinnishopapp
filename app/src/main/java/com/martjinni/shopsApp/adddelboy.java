package com.martjinni.shopsApp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonObjectRequest;
import com.martjinni.Appshops.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class adddelboy extends AppCompatActivity {

    final delboystep1 db1 = new delboystep1();
    final delboystep2 db2 = new delboystep2();
    final delboystep3 db3 = new delboystep3();

    public volatile boolean isWorking = false;
    public RequestQueue mRequestQueue;
    volleyClass newClass = new volleyClass();
    public String baseUrl = newClass.baseUrl;

    String musername = "";
    String password = "";
    String fullname = "";
    String email = "";
    String phonenumber = "";
    String Otp = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adddelboy);
        openFragment(db1);
    }

    private void openFragment(final Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.setCustomAnimations(android.R.anim.slide_out_right, android.R.anim.slide_in_left);
        transaction.replace(R.id.sessioncontainer, fragment);
        if (fragment == db1) {
            Log.i("TAg", "First FRagmnet");
        } else {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    public void validateusername(final String username, final String npassword) {
        try {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
            JSONObject jsonVal = new JSONObject();
            jsonVal.put("userName", username);
            jsonVal.put("mode", "client");
            String userUrl = baseUrl + "validateuser";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    musername = username;
                    password = npassword;
                    Toast.makeText(adddelboy.this, "Welcome " + username, Toast.LENGTH_SHORT).show();
                    openFragment(db2);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertshow("Username Not available, please try something else");
                    error.printStackTrace();
                }
            });
            mRequestQueue.add(jsonObjectRequest);
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
    }

    public void validatenumber(final String mobnumber, final String mfullname, final String memail) {
        try {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
            JSONObject jsonVal = new JSONObject();
            jsonVal.put("phoneNumber", Double.parseDouble(mobnumber));
            jsonVal.put("userType", "CUST");
            String userUrl = baseUrl + "validphone";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    //new
                    phonenumber = mobnumber;
                    email = memail;
                    fullname = mfullname;

                    Toast.makeText(adddelboy.this, "Generating OTP", Toast.LENGTH_LONG).show();
                    generateotp();
                    openFragment(db3);
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    alertshow("Mobile number already exists.");

                }
            });
            mRequestQueue.add(jsonObjectRequest);
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
    }

    public void generateotp() {
        isWorking = true;
        try {
            Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
            Network network = new BasicNetwork(new HurlStack());
            mRequestQueue = new RequestQueue(cache, network);
            mRequestQueue.start();
            JSONObject jsonVal = new JSONObject();
            jsonVal.put("phoneNumber", Double.parseDouble(phonenumber));
            jsonVal.put("userType", "DM");
            jsonVal.put("userNeed", "CREATE");
            String userUrl = baseUrl + "generateotp";
            JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Toast.makeText(adddelboy.this, "OTP generated", Toast.LENGTH_LONG).show();
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Toast.makeText(adddelboy.this, "Otp not generated" + error.toString(), Toast.LENGTH_LONG).show();
                    error.printStackTrace();
                }
            });
            mRequestQueue.add(jsonObjectRequest);
        } catch (JSONException jsonException) {
            jsonException.printStackTrace();
        }
    }

    public void validateotp(String notp) {
        if(isWorking){
            isWorking = false;
            try {
                Cache cache = new DiskBasedCache(getCacheDir(), 1024 * 1024);
                Network network = new BasicNetwork(new HurlStack());
                mRequestQueue = new RequestQueue(cache, network);
                mRequestQueue.start();
                JSONObject jsonVal = new JSONObject();
                SharedPreferences sharedPreferences = getSharedPreferences("userInfo", Context.MODE_PRIVATE);
                final JSONObject sessionInfo = new JSONObject (sharedPreferences.getString ("sessionInfo",""));
                jsonVal.put("otp", notp);
                jsonVal.put("userNameLogin", musername);
                jsonVal.put("password", password);
                jsonVal.put("phoneNumber", Double.parseDouble(phonenumber));
                jsonVal.put("dmName", fullname);
                jsonVal.put("emailId", email);
                jsonVal.put("userType", "DM");
                jsonVal.put("typeId",sessionInfo.getInt("typeId"));
                String userUrl = baseUrl + "createuser";
                JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, userUrl, jsonVal, new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Toast.makeText(adddelboy.this, "Deliveryboy added", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(adddelboy.this, deliveryBoy.class);
                        startActivity(intent);
                        finish();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(adddelboy.this, "Something wrong happened", Toast.LENGTH_LONG).show();
                        error.printStackTrace();
                        isWorking = true;
                    }
                }){
                    @Override
                    public Map getHeaders() throws AuthFailureError {
                        Map<String,String> params = new HashMap<>();
                        params.put("Content-Type", "application/json");
                        try{
                            params.put("basicAuthenticate", sessionInfo.getString ("basicAuthenticate"));
                        }
                        catch (JSONException jsonExcepion2){
                            jsonExcepion2.printStackTrace ();
                        }
                        return params;
                    }
                };
                mRequestQueue.add(jsonObjectRequest);
            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }

        }
        else{
            Toast.makeText (adddelboy.this, "Request is already in progress", Toast.LENGTH_SHORT).show ();
        }
    }

    public void alertshow(String message) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(adddelboy.this);
        View mView = getLayoutInflater().inflate(R.layout.alertdialog, null);
        TextView cancelbtn = mView.findViewById(R.id.modalContinue);
        cancelbtn.setVisibility(View.INVISIBLE);
        TextView messageshow = mView.findViewById(R.id.errormessage);
        messageshow.setText(message);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder.create();
        dialog.show();
        TextView close = mView.findViewById(R.id.modalclose);
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
    }


    public void backPressed(View view) {
        Intent intent = new Intent(adddelboy.this, deliveryBoy.class);
        startActivity(intent);
        finish();
    }
}
